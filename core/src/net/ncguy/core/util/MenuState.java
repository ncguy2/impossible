package net.ncguy.core.util;

import net.ncguy.commonutil.audio.MusicTracks;
import net.ncguy.core.screen.menu.*;

/**
 * Created by ncguy on 15/11/2014 at 14:21.
 */
public enum MenuState {
    GAME(null, MusicTracks.GAMEMISC),
    END(new EndLevelElement(), MusicTracks.END),
    MAINMENU(new MainMenuElement(), MusicTracks.MENU),
    LEVELOFFICIAL(new LevelSelectOElement(), MusicTracks.MENU),
    LEVELCOMMUNITY(new LevelSelectCElement(), MusicTracks.MENU),
    MENUOPTIONS(new OptionsElement(), MusicTracks.MENU),
    MENUHELP(new HelpMenuElement(), MusicTracks.MENU),
    LOBBY(new LobbyElement(), MusicTracks.MENU),
    ;

    private MenuState(IMenuElement element, MusicTracks track) {
        this.element = element;
        this.track = track;
    }

    private IMenuElement element;
    private MusicTracks track;

    public IMenuElement getElement() { return this.element; }
    public MusicTracks  getTracks()  { return this.track;   }

}
