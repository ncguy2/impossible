package net.ncguy.core.util;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by ncguy2 on 15/10/2014.
 */
public class CamUtils {

    public static Cam2 up = new Cam2(0, 0),
            down = new Cam2(1, 1),
            north = new Cam2(1, 0),
            east = new Cam2(0, 1),
            south = new Cam2(-1, 0),
            west = new Cam2(0, -1);

    public static Cam2 calculateGradient(Vector3 cam, float offset) {
        Cam2 result = new Cam2();
        float angle = getCurrentCameraAngle(cam)+offset;
        float adjacent = 0, opposite = 0;
        float hypotenuse = 1;
        opposite = MathUtils.sinDeg(angle)*hypotenuse;
        adjacent = (hypotenuse*hypotenuse)-(opposite*opposite);
        adjacent = (float)Math.sqrt(adjacent);
        result.x = adjacent;
        result.z = opposite;
        return result;
    }
    public static float getCurrentCameraAngle(Vector3 cam) {
        return(float)(Math.round(Math.atan2(cam.x, cam.z)*MathUtils.radiansToDegrees)+180f)%360f;
    }

    public static class Cam2 {
        public float x, z;
        public Cam2() { this(0, 0); }
        public Cam2(float x, float z) { this.x = x; this.z = z; }

        public Vector3 promote() {
            return promote(0);
        }
        public Vector3 promote(float y) {
            return new Vector3(x, y, z);
        }
        @Override
        public String toString() {
            return String.format("[%s, %s]", this.x, this.z);
        }
    }

}
