package net.ncguy.core.remote;

import net.ncguy.commonutil.handlers.LevelTranslation;
import net.ncguy.commonutil.util.LevelPreview;
import net.ncguy.commonutil.util.LevelType;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.net.URL;

/**
 * Created by ncguy on 16/12/2014 at 13:19.
 */
public class HTTPConnect {

    static LevelTranslation.LevelInfo world;

    public static LevelTranslation.LevelInfo connect(String url) {
        try {
            world = new LevelTranslation.LevelInfo();
            DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuild = docFac.newDocumentBuilder();
            Document doc = docBuild.parse(new URL(url).openStream());

            doc.getDocumentElement().normalize();
            String str = "Root Element: " + doc.getDocumentElement().getNodeName() + ", " + doc.getDocumentElement().getAttribute("id");
            System.out.println(str);
            NodeList partList = doc.getElementsByTagName("Part");
            for (int i = 0; i < str.length(); i++)
                System.out.print("-");
            System.out.print("\n");
            try {
                readParts(partList);
            } catch (Exception exc) {
            }
            NodeList triggerList = doc.getElementsByTagName("Trigger");
            for (int i = 0; i < str.length(); i++)
                System.out.print("-");
            System.out.print("\n");
            try {
                readTriggers(triggerList);
            } catch (Exception exc) {
            }
            System.out.println("Registered world parts: " + world.world.size());
            System.out.println("Registered world triggers: " + world.triggers.size());
            world.preview = new LevelPreview();
            world.preview.name = doc.getDocumentElement().getAttribute("id");
            world.preview.path = url;
            return world;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void readParts(NodeList partList) {
        for (int tmp = 0; tmp < partList.getLength(); tmp++) {
            Node part1 = partList.item(tmp);
            System.out.println("Current Element: Part with id: " + part1.getAttributes().getNamedItem("id").getNodeValue());
            if (part1.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) part1;
                try {
                    LevelTranslation.LevelPartInfo info = new LevelTranslation.LevelPartInfo();
                    info.partId = part1.getAttributes().getNamedItem("id").getNodeValue();
                    System.out.printf("\tObj: %s\n" +
                                    "\t\tSize: [%s, %s, %s]\n" +
                                    "\t\tMass: %s\n" +
                                    "\t\tColour: %s\n" +
                                    "\t\tAlpha: %s\n",
                            e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("shape").getNodeValue(),
                            e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("x").getNodeValue(),
                            e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("y").getNodeValue(),
                            e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("z").getNodeValue(),
                            e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("mass").getNodeValue(),
                            e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("colour").getNodeValue(),
                            e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("alpha").getNodeValue());
                    info.objShape = e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("shape").getNodeValue();
                    info.objX = e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("x").getNodeValue();
                    info.objY = e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("y").getNodeValue();
                    info.objZ = e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("z").getNodeValue();
                    info.objMass = e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("mass").getNodeValue();
                    info.objColour = e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("colour").getNodeValue();
                    try {
                        info.objAlpha = e.getElementsByTagName("Obj").item(0).getAttributes().getNamedItem("alpha").getNodeValue();
                    } catch (Exception ex) {
                        info.objAlpha = "1f";
                    }
                    System.out.printf("\tPos: [%s, %s, %s]\n",
                            e.getElementsByTagName("Pos").item(0).getAttributes().getNamedItem("x").getNodeValue(),
                            e.getElementsByTagName("Pos").item(0).getAttributes().getNamedItem("y").getNodeValue(),
                            e.getElementsByTagName("Pos").item(0).getAttributes().getNamedItem("z").getNodeValue());
                    info.posX = e.getElementsByTagName("Pos").item(0).getAttributes().getNamedItem("x").getNodeValue();
                    info.posY = e.getElementsByTagName("Pos").item(0).getAttributes().getNamedItem("y").getNodeValue();
                    info.posZ = e.getElementsByTagName("Pos").item(0).getAttributes().getNamedItem("z").getNodeValue();
                    System.out.printf("\tRot: [%s, %s, %s]\n",
                            e.getElementsByTagName("Rot").item(0).getAttributes().getNamedItem("roll").getNodeValue(),
                            e.getElementsByTagName("Rot").item(0).getAttributes().getNamedItem("pitch").getNodeValue(),
                            e.getElementsByTagName("Rot").item(0).getAttributes().getNamedItem("yaw").getNodeValue());
                    info.rotRoll = e.getElementsByTagName("Rot").item(0).getAttributes().getNamedItem("roll").getNodeValue();
                    info.rotPitch = e.getElementsByTagName("Rot").item(0).getAttributes().getNamedItem("pitch").getNodeValue();
                    info.rotYaw = e.getElementsByTagName("Rot").item(0).getAttributes().getNamedItem("yaw").getNodeValue();
                    System.out.printf("\tType: %s\n",
                            e.getElementsByTagName("Type").item(0).getAttributes().getNamedItem("id").getNodeValue());
                    info.typeId = e.getElementsByTagName("Type").item(0).getAttributes().getNamedItem("id").getNodeValue();
                    LevelTranslation.LevelPartTranslatedInfo transinfo = new LevelTranslation.LevelPartTranslatedInfo(info);
                    world.newPart(transinfo.translate());
                } catch (Exception p) {
                    p.printStackTrace();
                }
            }
        }
    }

    public static void readTriggers(NodeList trigList) {
        for (int tmp = 0; tmp < trigList.getLength(); tmp++) {
            Node part1 = trigList.item(tmp);
            System.out.println("Current Element: Part with id: " + part1.getAttributes().getNamedItem("id").getNodeValue());
            if (part1.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) part1;
                try {
                    LevelTranslation.TriggerInfo info = new LevelTranslation.TriggerInfo();
                    info.triggerId = e.getAttributes().getNamedItem("id").getNodeValue();
                    info.type = e.getElementsByTagName("Type").item(0).getAttributes().getNamedItem("id").getNodeValue();
                    NamedNodeMap dims = e.getElementsByTagName("Shape").item(0).getAttributes();
                    info.ax = dims.getNamedItem("ax").getNodeValue();
                    info.ay = dims.getNamedItem("ay").getNodeValue();
                    info.az = dims.getNamedItem("az").getNodeValue();
                    info.bx = dims.getNamedItem("bx").getNodeValue();
                    info.by = dims.getNamedItem("by").getNodeValue();
                    info.bz = dims.getNamedItem("bz").getNodeValue();
                    LevelTranslation.TriggerTranslatedInfo transInfo = new LevelTranslation.TriggerTranslatedInfo(info);
                    world.newTrigger(transInfo.translate());
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        }
    }

    public static LevelPreview getLevelData(String f) {
        LevelPreview info = new LevelPreview();
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document d = db.parse(new URL(f).openStream());

            d.getDocumentElement().normalize();
            info.path = f;
            info.name = d.getDocumentElement().getAttribute("id");
            info.imgName = d.getDocumentElement().getAttribute("image");
            int index = 0;
            String type = d.getDocumentElement().getAttribute("type");
            while(info.type == null) {
                if(index >= LevelType.values().length) info.type = LevelType.UNKNOWN;
                if(type.toLowerCase().equals(LevelType.values()[index].name().toLowerCase()))
                    info.type = LevelType.values()[index];
                index++;
            }

        }catch(Exception e) {}
        return info;
    }

}