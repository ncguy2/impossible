package net.ncguy.core.listener;

import com.badlogic.gdx.Gdx;
import net.ncguy.commonutil.Persist;
import net.ncguy.commonutil.util.patch.Patch;
import net.ncguy.commonutil.util.Tracker;
import net.ncguy.core.Main;
import net.ncguy.core.util.MenuState;
import net.ncguy.core.screen.GameScreen;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.core.screen.menu.BaseMenuElement;
import net.ncguy.core.screen.menu.LevelSelectOElement;

/**
 * Created by ncguy on 15/11/2014 at 16:43.
 */
public class EventListener {

    private static Patch patch;

    public static void buttonListener() {
        MenuState state = MenuScreen.currentState;
        if(state == MenuState.MAINMENU)
            processMainMenu();
        if(state == MenuState.LEVELOFFICIAL)
            processLevelOfficial();
        if(state == MenuState.END)
            processEndLevel();
        if(state == MenuState.MENUOPTIONS)
            processOptions();
    }

    private static void processMainMenu() {
        //Patch0
        patch = BaseMenuElement.patch0;
        if(patch.isButton) if(Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                Persist.options.saveFile();
                Gdx.app.exit();
            }
        }
        //Patch 1
        patch = BaseMenuElement.patch1;
        if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if (Gdx.input.isTouched()) {
                MenuScreen.changeState(MenuState.LEVELOFFICIAL);
            }
        }
        //Patch 2
        patch = BaseMenuElement.patch2;
        if(patch.isButton) if(Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                MenuScreen.changeState(MenuState.MENUOPTIONS);
            }
        }
        //Patch 3
        patch = BaseMenuElement.patch3;
        if(patch.isButton) if(Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                MenuScreen.changeState(MenuState.MENUHELP);
            }
        }
    }
    private static void processLevelOfficial() {
        //Patch0
        patch = BaseMenuElement.patch0;
        if(patch.isButton) if(Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                MenuScreen.changeState(MenuState.MAINMENU);
            }
        }
        if (LevelSelectOElement.prevToggle) {
            //Patch 1
            patch = BaseMenuElement.patch1;
            if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
                if (Gdx.input.isTouched()) {
                    int index = LevelSelectOElement.selectedIndex;
                    index--;
                    if (index < 0) index = 0;
                    LevelSelectOElement.selectedIndex = index;
                    LevelSelectOElement.prevToggle = false;
                }
            }
            //Patch5
            patch = BaseMenuElement.patch5;
            if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
                if (Gdx.input.isTouched()) {
                    int index = LevelSelectOElement.selectedIndex;
                    index -= 5;
                    if (index < 0) index = 0;
                    LevelSelectOElement.selectedIndex = index;
                    LevelSelectOElement.prevToggle = false;
                }
            }
            //Patch7
            patch = BaseMenuElement.patch7;
            if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
                if (Gdx.input.isTouched()) {
                    LevelSelectOElement.selectedIndex = 0;
                    LevelSelectOElement.prevToggle = false;
                }
            }
        }
        if (LevelSelectOElement.nextToggle) {
            //Patch2
            patch = BaseMenuElement.patch2;
            if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
                if (Gdx.input.isTouched()) {
                    int index = LevelSelectOElement.selectedIndex;
                    index++;
                    if (index >= LevelSelectOElement.levels.size) index = LevelSelectOElement.levels.size - 1;
                    LevelSelectOElement.selectedIndex = index;
                    LevelSelectOElement.nextToggle = false;
                }
            }
            //Patch6
            patch = BaseMenuElement.patch6;
            if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
                if (Gdx.input.isTouched()) {
                    int index = LevelSelectOElement.selectedIndex;
                    index += 5;
                    if (index >= LevelSelectOElement.levels.size) index = LevelSelectOElement.levels.size - 1;
                    LevelSelectOElement.selectedIndex = index;
                    LevelSelectOElement.nextToggle = false;
                }
            }
            //Patch8
            patch = BaseMenuElement.patch8;
            if(patch.isButton)if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
                if (Gdx.input.isTouched()) {
                    LevelSelectOElement.selectedIndex = LevelSelectOElement.levels.size - 1;
                    LevelSelectOElement.nextToggle = false;
                }
            }
        }
        //Patch4
        patch = BaseMenuElement.patch4;
        if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if (Gdx.input.isTouched()) {
                Main.level = LevelSelectOElement.levels.get(LevelSelectOElement.selectedIndex).path;
                MenuScreen.game.setScreen(new GameScreen(MenuScreen.game));
            }
        }
    }
    private static void processEndLevel() {
        //Patch1
        patch = BaseMenuElement.patch1;
        if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                MenuScreen.game.setScreen(new GameScreen(MenuScreen.game));
            }
        }
        //Patch2
        patch = BaseMenuElement.patch2;
        if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                MenuScreen.changeState(MenuState.MAINMENU);
            }
        }
        //Patch3
        patch = BaseMenuElement.patch3;
        if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                MenuScreen.changeState(MenuState.LEVELOFFICIAL);
            }
        }
    }
    private static void processOptions() {
        //Patch0
        patch = BaseMenuElement.patch0;
        if(patch.isButton) if (Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                Persist.options.saveFile();
                MenuScreen.changeState(MenuState.MAINMENU);
            }
        }
        //Patch4
        patch = BaseMenuElement.patch4;
        if(patch.isButton) if(Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                Persist.options.tweakMusicVolume(-1);
                MenuScreen.currentMusic.setVolume(Persist.options.getMusicVolume()/100f);
            }
        }
        //Patch5
        patch = BaseMenuElement.patch5;
        if(patch.isButton) if(Tracker.hoverOverRegion(patch.pos, patch.size)) {
            if(Gdx.input.isTouched()) {
                Persist.options.tweakMusicVolume(1);
                MenuScreen.currentMusic.setVolume(Persist.options.getMusicVolume());
            }
        }
    }
}
