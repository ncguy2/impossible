package net.ncguy.core;

import com.badlogic.gdx.Game;
import net.ncguy.commonutil.Preferences;
import net.ncguy.commonutil.api.plugins.PluginLoader;
import net.ncguy.commonutil.assets.Assets;
import net.ncguy.commonutil.particle.ParticleEmitter;
import net.ncguy.commonutil.signal.NetworkInit;
import net.ncguy.commonutil.skin.VisUIWrapper;
import net.ncguy.commonutil.util.Startup;
import net.ncguy.core.screen.GameScreen;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.utils.stream.MultiOutputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Main extends Game {

    public static String level = "core/assets/levels/Lvl_Test_Debug_42-69a.world";
    public static boolean isDebug = false;
    public static boolean direct = false;
    public static boolean hasShader = false;

    @Override
    public void create() {
        VisUIWrapper.load();
        net.ncguy.commonutil.assets.Assets.load();
        PluginLoader.load();
        ParticleEmitter.AngleController.init();
        Startup.init();
        NetworkInit.init();
        PluginLoader.run();
        try{
            File log = new File("log");
            if(!log.exists())
                log.mkdirs();
            FileOutputStream fout = new FileOutputStream("log/engine.out.log");
            FileOutputStream ferr = new FileOutputStream("log/engine.err.log");
            MultiOutputStream mout = new MultiOutputStream(System.out, fout);
            MultiOutputStream merr = new MultiOutputStream(System.err, ferr);
            PrintStream pout = new PrintStream(mout);
            PrintStream perr = new PrintStream(merr);
            System.setOut(pout);
            System.setErr(perr);
        }catch(Exception e) {}
//        AssetList.init();
        this.setScreen(new MenuScreen(this));
    }
}

