package net.ncguy.core.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import net.ncguy.commonutil.assets.GifLoader;
import net.ncguy.core.Main;

/**
 * Created by Nick on 09/03/2015 at 16:25.
 * Project: Development
 * Package: net.ncguy.core.screen
 */
public class LoadingScreen implements Screen {

    Main root;
    Screen target;
    SpriteBatch batch;
    OrthographicCamera cam;
    public static boolean canProceed = false;

    public LoadingScreen(Main root, Screen target) {
        this.root = root;
        this.target = target;

        batch = new SpriteBatch();
        cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        canProceed = false;

//        loadAnim = GifLoader.loadGIFAnimation(Animation.PlayMode.LOOP.ordinal(), Gdx.files.internal("core/assets/gif/loading.gif").read());
    }

    public void proceedToTarget() {
        this.root.setScreen(this.target);
    }

//    Animation loadAnim;
//    TextureRegion drawLoadAnim;
    float frameCounter = 0;

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.15f, .15f, .15f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        frameCounter+=delta;
        batch.setProjectionMatrix(cam.combined);
        batch.setTransformMatrix(cam.combined);
        batch.begin();
//        drawLoadAnim = loadAnim.getKeyFrame(frameCounter, true);
//        System.out.println(drawLoadAnim.getTexture().toString());
//        batch.draw(drawLoadAnim, (Gdx.graphics.getWidth()/2)-(drawLoadAnim.getRegionWidth()), -(Gdx.graphics.getHeight()/2));
//        batch.draw(drawLoadAnim, 0, 0);
        batch.end();
        cam.update();
//        if(canProceed) proceedToTarget();
    }

    @Override
    public void resize(int width, int height) {
        cam.viewportWidth = width;
        cam.viewportHeight = height;
    }

    @Override public void show() {}
    @Override public void pause() {}
    @Override public void resume() {}
    @Override public void hide() {}
    @Override public void dispose() {}
}
