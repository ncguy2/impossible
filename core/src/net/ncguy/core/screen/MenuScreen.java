package net.ncguy.core.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.kotcrab.vis.ui.VisUI;
import net.ncguy.commonutil.Persist;
import net.ncguy.commonutil.screen.AbstractScreen;
import net.ncguy.core.Main;
import net.ncguy.core.screen.menu.BaseMenuElement;
import net.ncguy.core.util.MenuState;
import net.ncguy.core.listener.EventListener;
import net.ncguy.core.screen.menu.IMenuElement;

/**
 * Created by ncguy2 on 20/09/2014.
 */
public class MenuScreen extends AbstractScreen {

    public static Main game;
    public static MenuState currentState, previousState;
    public static IMenuElement currentElement;
    public static boolean changeState = false, changeStateToggle = true;
    public static float changeStateDelta = 0.2f;
    public static float previousAlpha = 0;

    public MenuScreen(Main game) {
        this(game, MenuState.MAINMENU);
        VisUI.load();
    }

    public MenuScreen(Main game, MenuState state) {
        if(currentMusic != null) if(currentMusic.isPlaying()) currentMusic.stop();
        this.game = game;
        init();
        currentState = state;
        previousState = null;
        currentElement = currentState.getElement();
        currentElement.onInit(this);
        Gdx.input.setCursorCatched(false);
        Gdx.input.setCursorPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        currentTrack = state.getTracks();
        currentMusic = currentTrack.getMusic();
        currentMusic.setLooping(true);
        currentMusic.setVolume(Persist.options.musicVolume/100);
        currentMusic.play();
    }

    @Override
    public void render(float delta) {
        if(!currentMusic.isPlaying()) {
            currentMusic.setVolume(Persist.options.musicVolume/100);
            currentMusic.play();
        }
        if(Main.direct)game.setScreen(new GameScreen(game));
        Gdx.gl.glClearColor(0, .6f, .6f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        //2D Render Starts Here
//        spriteBatch.begin();

        if (changeState) {
            currentElement.onInit(this);
            changeState = false;
        }

        currentElement.onRender(delta, this);

        if (previousAlpha > 0) {
            renderPrevious(delta);
        }

//        spriteBatch.end();
        //2D Render Ends Here

        if(changeStateToggle) {
            if(keyPress(Input.Keys.NUMPAD_1)) changeState(MenuState.MAINMENU);
            if(keyPress(Input.Keys.NUMPAD_2)) changeState(MenuState.LEVELOFFICIAL);
            if(keyPress(Input.Keys.NUMPAD_3)) game.setScreen(new LoadingScreen(game, null));
        }
        processToggleState(delta);
        if(keyPress(Input.Keys.END)) {
            changeState(MenuState.GAME);
            game.setScreen(new GameScreen(game));
        }

        spriteBatch.begin();
        font.draw(spriteBatch, "FPS: "+Gdx.graphics.getFramesPerSecond(), 100, 100);
        spriteBatch.end();

//        EventListener.buttonListener();
    }

    public static void changeState(MenuState newState) {
        BaseMenuElement.isUsingStage = false;
        changeStateToggle = false;
        previousState = currentState;
        currentState = newState;
        changeState = true;
        currentElement = currentState.getElement();
        previousAlpha = 1;
        changeTrack(currentState.getTracks());
    }

    public boolean keyPress(int key) {
        return Gdx.input.isKeyPressed(key);
    }

    public void renderPrevious(float delta) {
        if(previousState != MenuState.GAME && previousState != null) {
            previousState.getElement().onRender(delta, this);
            previousAlpha -= 0.15f;
        }
    }

    private void processToggleState(float delta) {
        if(!changeStateToggle) {
            changeStateDelta -= delta;
            if(changeStateDelta <= 0) {
                changeStateToggle = true;
                changeStateDelta = 0.2f;
            }
        }
    }

    @Override public void resize(int width, int height) {
        w = width; h = height;
    }


}
