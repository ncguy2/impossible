package net.ncguy.core.screen;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalShadowLight;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.g3d.model.data.ModelData;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DefaultTextureBinder;
import com.badlogic.gdx.graphics.g3d.utils.DepthShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.DebugDrawer;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.physics.bullet.dynamics.*;
import com.badlogic.gdx.physics.bullet.linearmath.btIDebugDraw;
import com.badlogic.gdx.utils.Array;

import net.ncguy.commonutil.audio.MusicTracks;
import net.ncguy.commonutil.factory.ModelFactory;
import net.ncguy.commonutil.factory.ShapeFactory;
import net.ncguy.commonutil.factory.TempGameObject;
import net.ncguy.commonutil.handlers.prefab.PrefabHandler;
import net.ncguy.commonutil.luaj.active.LuaHandler;
import net.ncguy.commonutil.luaj.tests.RunLineData;
import net.ncguy.commonutil.model.ModelInst;
import net.ncguy.commonutil.particle.ParticleController;
import net.ncguy.commonutil.particle.ParticleFactory;
import net.ncguy.commonutil.render.shader.Shader;
import net.ncguy.commonutil.render.shader.ShaderProvider;
import net.ncguy.commonutil.screen.AbstractScreen;
import net.ncguy.commonutil.signal.GenericEmitter;
import net.ncguy.commonutil.signal.GenericReceiver;
import net.ncguy.commonutil.signal.Network;
import net.ncguy.commonutil.trigger.EnumTriggers;
import net.ncguy.commonutil.trigger.triggers.*;
import net.ncguy.commonutil.trigger.TriggerArea;
import net.ncguy.commonutil.util.Timer;
import net.ncguy.core.Main;
import net.ncguy.commonutil.handlers.BaseContactListener;
import net.ncguy.commonutil.assets.AssetManager;
import net.ncguy.commonutil.factory.GameObject;
import net.ncguy.commonutil.handlers.*;
import net.ncguy.commonutil.model.ModelBuilder;
import net.ncguy.commonutil.model.ModelInfo;
import net.ncguy.core.remote.HTTPConnect;
import net.ncguy.core.util.MenuState;
import net.ncguy.core.player.PlayerObject;
import net.ncguy.commonutil.screen.camera.FPCameraController;
import net.ncguy.core.util.CamUtils;
import net.ncguy.commonutil.util.Flags;
import net.ncguy.commonutil.util.ModelInstanceData;
import net.ncguy.utils.obj.Float3;
import net.ncguy.utils.obj.Object3;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ncguy2 on 20/09/2014.
 */
public class GameScreen extends AbstractScreen {

    boolean inverseGrav = false;
    Main game;
    RenderContext context;
    Shader shader;
    ShaderProvider sp;
    Renderable renderable;
    com.badlogic.gdx.graphics.g3d.utils.ModelBuilder mb;
    ModelFactory mf;

    ModelInstance torusTest;

    public float slowFactor = 3f;

//    ShaderManager sm;
//    AssetManager am = new AssetManager();
//    Matrix4 projection = new Matrix4();
//    Matrix4 view = new Matrix4();
//    Matrix4 mdl = new Matrix4();
//    Matrix4 combined = new Matrix4();
//    Vector3 axis = new Vector3(1, 0, 1).nor();
    float ang = 45f;

    Vector3 north = new Vector3(100, 0, 0), south = new Vector3(-100, 0, 0), east = new Vector3(0, 0, .2f), west =
            new Vector3(0, 0, -.2f);
    float spawnTimer;

    DirectionalShadowLight shadowLight;
    ModelBatch shadowBatch;

    PerspectiveCamera pipCam;

    public GameScreen(Main game) {
        this.game = game;
        initLuaPools();
        AssetManager.printFiles(new File("core/assets/levels"));
//        LevelLoader.readLevelFile(new File("core/net.ncguy.commonutil.assets/levels/level0.world"));
        Bullet.init();
        defaultGravity = new Vector3(0, -10, 0);
        inverseGravity = new Vector3(0,  10, 0);
        modelInstances = new Array<>();


        ModelData modelData;
        Model modelTest;

        mb = new com.badlogic.gdx.graphics.g3d.utils.ModelBuilder();
        mf = new ModelFactory(mb);

//        for(int i = 0; i < modelTest.nodes.size; i++)
//            modelInstances.add(ModelLoading.buildModelMesh(modelTest, modelTest.nodes.get(i)));

//        ModelLoader loader = new G3dModelLoader(new JsonReader());
//        File modelPath = new File("core/assets/models/start.g3dj");
//        modelData = loader.loadModelData(Gdx.files.absolute(modelPath.getAbsolutePath()));
//        modelTest = new Model(modelData, new TextureProvider.FileTextureProvider());
//        btCollisionShape collisionShape = Bullet.obtainStaticNodeShape(modelTest.nodes);
//
//        GameObject.Constructor constr = new GameObject.Constructor(modelTest, "model", collisionShape, 0, modelPart);
//        for(int i = 0; i < modelTest.nodes.size; i++) {
//            ModelInstance modelInst = ModelLoading.buildModelMesh(modelTest, modelTest.nodes.get(i));
//            modelInst.transform.setFromEulerAngles(modelPart.rot.x, modelPart.rot.y, modelPart.rot.z);
//            modelInst.transform.trn(modelPart.pos);
//            modelInst.transform.scl(modelPart.size);
//            modelInstances.add(modelInst);
//        }

        super.init();
        if(Main.hasShader)
            super.initShader();

        //World Stuff
        env.set(new ColorAttribute(ColorAttribute.AmbientLight, .4f, .4f, .4f, 1));
        env.add(new DirectionalLight().set(.8f, .8f, .8f, -1, -.8f, -.2f));
        env.add(new DirectionalLight().set(.2f, .8f, .8f, -.7f, .2f, 1));
        env.add(new PointLight().set(Color.RED, 0, 1, 0, 1));
        env.add((shadowLight = new DirectionalShadowLight(1024, 1024, 60, 60, .1f, 50)).set(1, 1, 1, 40, -35, -35));
        env.shadowMap = shadowLight;

        shadowBatch = new ModelBatch(new DepthShaderProvider());

        //Camera Stuff
        cam = new PerspectiveCamera(90, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(3, 7, 10);
        cam.lookAt(0, 4, 0);
        cam.near = 0.1f;
        cam.far = 1000f;
        cam.update();
        pipCam = new PerspectiveCamera(90, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        pipCam.position.set(25, 25, 25);
        pipCam.lookAt(0, 4, 0);
        pipCam.near = .1f;
        pipCam.far = 1000;
        pipCam.update();

        camController = new CameraInputController(cam);
        fpCamControl = new FPCameraController(cam);
        Gdx.input.setInputProcessor(fpCamControl);

        if(Main.level.startsWith("http")) {
            info = HTTPConnect.connect(Main.level);
        }else if(Main.level.endsWith(".wrl")){
            info = new LevelTranslation.LevelInfo().deserialize(new File(Main.level));
        }else{
            info = LevelLoader.readLevelFile(new File(Main.level));
        }
        show();

//        LevelTranslation.LevelInfo info = HTTPConnect.connect("http://pastebin.com/raw.php?i=6MuZm93g");
//        LevelTranslation.LevelInfo info = LevelLoader.readLevelFile(new File("core/assets/levels/test2.world"));

//        torusTest = ShapeFactory.createTorus(4, 0, -20, 0, 25, 5, 25, 25, 1, 1, 0, .7f);

//        initParticles();
        particleController = ParticleController.build(cam);
        for(ParticleFactory.ParticleTranslated p : info.particles)
            particleController.addEmitter(p);
//        particleController.addEmitter(new float[]{.12f, 1, .05f}, new Vector3(0,5,-5));
//        particleController.addEmitter(new float[]{.12f, .05f, 1}, new Vector3(0,5,5));
//        particleController.addEmitter(new float[]{1, 1, 0}, new Vector3(25, 200, 17));
    }

//    GameObject torus;
    float angle, speed = 90f;
    Float3 angles, speeds;
    CamUtils.Cam2 gradient;
    float circleSpeed = 5, forwardSpeed = .05f;
    float circleSize = 1, circleGrowSpeed = .01f;
    float z1 = -10;

    @Override
    public void render(float delta) {
        speed = 2f;
        try{
            cfg.obtain();
            dispatcher.obtain();
            broadphase.obtain();
            constraintSolver.obtain();
            world.obtain();
            drawer.obtain();
            contactListener.obtain();
        }catch(Exception e) {
            e.printStackTrace();
        }
        try {
            deltaTime = delta;
            time.onUpdate(delta);
            angle = (angle + delta * speed) % 360f;
            speeds.a = 2;
            speeds.b = 3;
            speeds.c = 5;
            angles.a = ((angles.a + delta * speeds.a)) % 360;
            angles.b = ((angles.b - delta * speeds.b)) % 360;
            angles.c = ((angles.c + delta * speeds.c)) % 360;

            float x1 = (float)(Math.sin(angle*circleSpeed)*20);
            float y1 = (float)(Math.cos(angle*circleSpeed)*20);
//            z1 += forwardSpeed*delta;
//            circleSize += circleGrowSpeed;

//            particleController.emitters().get(0).controller.transform.setToTranslation(x1, y1, z1);
//      instances.get(0).transform.setFromEulerAngles(MathUtils.sinDeg(angle * (2+5)), MathUtils.sinDeg(angle * (5+5)), MathUtils.sinDeg(angle * (3+5)));
//      instances.get(0).transform.trn(0, MathUtils.sinDeg(angle), 0);
//      instances.get(0).body.setWorldTransform(instances.get(0).transform);
            world.stepSimulation(delta, 5, 1f / 60f);

            if ((spawnTimer -= delta) < 0) {
//          spawn();
                spawnTimer = 1.5f;
            }

            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClearColor(.2f, .2f, .2f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT|GL20.GL_DEPTH_BUFFER_BIT);
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            Gdx.gl.glFrontFace(GL20.GL_CCW);

            shadowPrepare();
            shadowLight.begin(new Vector3(cam.position.x+10, 0, 0), cam.direction);
            shadowBatch.begin(shadowLight.getCamera());
//            shadowBatch.render(instances);
            for(GameObject obj : instances) {
                shadowBatch.render(obj, env);
            }
//            shadowBatch.render(playerInst);
            shadowBatch.end();
            shadowLight.end();
            shadowRecover();
            if (Main.isDebug) {
                drawer.begin(cam);
                world.debugDrawWorld();
                drawer.end();
            }
            renderObjects(delta);
            timer += delta;
            timer %= 360f;

            camController.update();
            fpCamControl.update();
            if(batch.getCamera() != null) {
                batch.end();
            }
            batch.begin(cam);
//            batch.render(torusTest, env);
            batch.render(ParticleController.build(cam).render(angle, delta), env);
//            System.out.println(ParticleController.build(cam).emitters().size);


//      instances.get(1).transform.setFromEulerAngles(MathUtils.sinDeg(angle * 2), MathUtils.sinDeg(angle * 5), MathUtils.sinDeg(angle * 3));
//      instances.get(1).transform.trn(0, MathUtils.sinDeg(angle), 0);
//      instances.get(1).body.setWorldTransform(instances.get(0).transform);

            showText = false;
            textToShow = "";
            activeTriggers.clear();
            inactiveTriggers.clear();
            for (TriggerArea trigger : triggers) {
                if (Main.isDebug || trigger.type == EnumTriggers.BROKEN) trigger.onRender();
                if (trigger.processDetection(playerInst.get(0))) activeTriggers.add(trigger);
                else inactiveTriggers.add(trigger);
                trigger.onPersist();
            }
//        shader.end();
//        context.end();
            batch.end();

            if(spriteBatch.isDrawing())
                spriteBatch.end();
            spriteBatch.begin();

            if (isEnd) {
                float diff = (Gdx.graphics.getHeight() - endSprite.getHeight());
                if (endSprite.getHeight() < Gdx.graphics.getHeight() + 20)
                    endSprite.setBounds(-5, (Gdx.graphics.getHeight() + 10) - endSprite.getHeight(),
                            Gdx.graphics.getWidth() + 10, endSprite.getHeight() + 5f);
                if (endSprite.getHeight() >= Gdx.graphics.getHeight() + 20) {
                    for(GameObject obj : instances) {
                        world.removeRigidBody(obj.body);
                        obj.body.release();
                    }
                    for(GameObject obj : playerInst) {
                        world.removeRigidBody(obj.body);
                        obj.body.release();
                    }
                    for(TempGameObject obj : tempInstances) {
                        world.removeRigidBody(obj.getObj().body);
                        obj.getObj().body.release();
                    }
                    world.release();
                    MenuScreen screen = new MenuScreen(game, MenuState.END);
                    game.setScreen(screen);
                }
            }

            if (activeTriggers.size > 0) showText = true;

            textToShow = "{";
            for (TriggerArea trigger : activeTriggers) {
                textToShow = textToShow + trigger.id + ", ";
                trigger.onProcess();
            }
            textToShow = textToShow + "} Triggers are Processing";
            textToShow = textToShow.replace(", } ", "} ");
            for (TriggerArea trigger : inactiveTriggers) trigger.onFalseProcess();

            try {
                endSprite.draw(spriteBatch);
            } catch (Exception exc) {}
            if (isDead) {
                deathSprite.draw(spriteBatch);
                respawnTimer += delta;
            }
            if (respawnTimer >= 3) {
                respawnTimer = 0;
                isDead = false;
                world.removeRigidBody(playerInst.get(0).body);
                playerInst.get(0).transform.setToTranslation(spawn);
                playerInst.get(0).body.setWorldTransform(playerInst.get(0).transform);
                playerInst.get(0).body.setLinearVelocity(new Vector3(0, 0, 0));
                playerInst.get(0).body.setAngularVelocity(new Vector3(0, 0, 0));
                playerInst.get(0).body.clearForces();
                world.clearForces();
//            world.setGravity(gravity);
                world.addRigidBody(playerInst.get(0).body);
            }
            playerInst.get(0).body.setAngularVelocity(new Vector3(0, 0, 0));

            try {
                if (showText) {
                    textPos.x = (Gdx.graphics.getWidth() / 2) - (font.getBounds(textToShow).width / 2);
                    textPos.y = (Gdx.graphics.getHeight() / 5) * 4;
                    font.draw(spriteBatch, textToShow, textPos.x, textPos.y);
//            System.out.println(textToShow+": ["+textPos.x+", "+textPos.y+"]");
                }

                textPos.x = (Gdx.graphics.getWidth() / 2) - (font.getBounds(time.toString()).width / 2);
                textPos.y = ((Gdx.graphics.getHeight() / 5) * 4) - 20;
                font.draw(spriteBatch, time.toString(), textPos.x, textPos.y);
            }catch (Exception e) {
                System.out.println("GameScreen.render");
                e.printStackTrace();
            }

            try {
                if (Main.isDebug) {
                    font.draw(spriteBatch, "Linear Velocity: " + playerInst.get(0).body.getInterpolationLinearVelocity(), 100, 360);

                    font.draw(spriteBatch, "North Gradient: " + CamUtils.calculateGradient(cam.direction, 0).toString(), 100, 300);
                    font.draw(spriteBatch, "South Gradient: " + CamUtils.calculateGradient(cam.direction, 180).toString(), 100, 280);
                    font.draw(spriteBatch, "East Gradient: " + CamUtils.calculateGradient(cam.direction, 90).toString(), 100, 260);
                    font.draw(spriteBatch, "West Gradient: " + CamUtils.calculateGradient(cam.direction, 270).toString(), 100, 240);

                    font.draw(spriteBatch, "Angle: " + CamUtils.getCurrentCameraAngle(cam.direction), 100, 200);
                    font.draw(spriteBatch, "xAngle: " + (Math.round(Math.atan2(cam.up.x, cam.up.z) * MathUtils.radiansToDegrees)), 100, 180);
                    font.draw(spriteBatch, "yAngle: " + (Math.round(Math.atan2(cam.up.y, cam.up.z) * MathUtils.radiansToDegrees)), 100, 160);
                    font.draw(spriteBatch, "zAngle: " + (Math.round(Math.atan2(cam.up.z, cam.up.z) * MathUtils.radiansToDegrees)), 100, 140);

                    font.draw(spriteBatch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 100, 100);
                    font.draw(spriteBatch, "Shapes: " + instances.size, 100, 80);
                    font.draw(spriteBatch, "Spawn Timer: " + spawnTimer, 100, 60);

                    if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)) {
                        float y = 60;
                        String str = "";
                        for (TriggerArea trigger : triggers) {
                            str = trigger.id + "[" + trigger.type.name() + "]";
                            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT))
                                str = str + " {" + trigger.processDetection(playerInst.get(0)) + "}";

                            font.draw(spriteBatch, str, 1020, y);
                            y += 20;
                        }
                        font.draw(spriteBatch, "Triggers: ", 1000, y);
                        y += 60;

                        str = "";
                        inverseGrav = false;
                        for (TriggerArea trigger : activeTriggers) {
                            str = trigger.id + "[" + trigger.type.name() + "]";
                            font.draw(spriteBatch, str, 1020, y);
                            y += 20;
                            if (trigger.type == EnumTriggers.GRAVITY) inverseGrav = true;
                        }
                        font.draw(spriteBatch, "Active Triggers: ", 1000, y);
                    }
                }
            }catch (Exception e) {
                System.out.println("GameScreen.render");
                e.printStackTrace();
            }
            if (!inverseGrav) gravity = new Vector3(0, -10, 0);
            else gravity = inverseGravity.cpy();

            world.setGravity(gravity);

            if(!spriteBatch.isDrawing())
                spriteBatch.begin();
            spriteBatch.end();

            alterDirections(delta);
            if (!isDead)
                playerControls();

            if (jumping) {
//                lin = playerInst.get(0).body.getLinearVelocity();
////                playerInst.get(0).transform.trn(0, .2f, 0);
//                Vector3 jumpVel = new Vector3();
//                jumpVel.set(-gravity.x, -gravity.y, -gravity.z);
//                jumpVel.scl(1/jumpCap);
////                if(jumpValue != 0)
////                    jumpVel.scl(jumpCap-jumpValue);
//                playerInst.get(0).transform.trn(jumpVel);
//                playerInst.get(0).body.setWorldTransform(playerInst.get(0).transform);
                jumpValue += 1;
            }
//
            if (jumpValue >= 30) {
                jumping = false;
                jumpValue = 0f;
            }

            updateCamera();
//            Gdx.gl.glViewport(Gdx.graphics.getWidth()-261, Gdx.graphics.getHeight()-197, 256, 192);
//            batch.begin(pipCam);
//            renderObjects(delta);
//            particleController.render(delta);
//            batch.end();
        }catch(Exception all) {
            all.printStackTrace();
        }
    }

    float jumpValue = 0, jumpCap = 25;
    Vector3 lin;

    private void renderObjects(float delta) {
        for(Network n : networks) {
            n.update();
        }
        for (int i = 0; i < instances.size; i++) {
            instances.get(i).render(batch, env, delta);
        }
        for (int i = 0; i < prefabInstances.size; i++) {
            batch.render(prefabInstances.get(i).getInstance(), env);
        }
        for (int i = 0; i < tempInstances.size; i++) {
            batch.render(tempInstances.get(i).getObj(), env);
        }
        for (int i = 0; i < tempInstances.size; i++) {
            if(tempInstances.get(i).update(delta)) {
                world.removeRigidBody(tempInstances.get(i).getObj().body);
                tempInstances.removeIndex(i);
            }
        }
        for (int i = 0; i < playerInst.size; i++) batch.render(playerInst.get(i), env);
        for (int i = 0; i < modelInstances.size; i++) {
            ModelInstance inst = modelInstances.get(i);
            batch.render(inst, env);
        }
    }

    public void alterDirections(float delta) {
        //FIXME
        float angle = CamUtils.getCurrentCameraAngle(cam.direction);
        Vector3 gradient = CamUtils.calculateGradient(cam.direction, 0).promote();

        if(angle > 270f && angle < 360f) {
            north = new Vector3(-gradient.z, 0, -gradient.x);
            east = new Vector3(gradient.x, 0, -gradient.z);
            south = new Vector3(gradient.z, 0, gradient.x);
            west = new Vector3(-gradient.x, 0, gradient.z);
        }else if(angle > 180f && angle < 270f) {
            west = new Vector3((gradient.x), 0, (gradient.z));
            east = new Vector3((-gradient.x), 0, (-gradient.z));
            north = new Vector3((-gradient.z), 0, (gradient.x));
            south = new Vector3((gradient.z), 0, (-gradient.x));
        }else if(angle > 90f && angle < 180f) {
            east = new Vector3((-gradient.x), 0, (-gradient.z));
            west = new Vector3((gradient.x), 0, (gradient.z));
            north = new Vector3((-gradient.z), 0, (gradient.x));
            south = new Vector3((gradient.z), 0, (-gradient.x));
        }else if(angle > 0f && angle < 90f) {
            north = new Vector3((-gradient.z), 0, (-gradient.x));
            south = new Vector3((gradient.z), 0, (gradient.x));
            west = new Vector3((-gradient.x), 0, (gradient.z));
            east = new Vector3((gradient.x), 0, (-gradient.z));
        }else if(angle == 0f) {
            north = new Vector3( 0, 0, -1);
            south = new Vector3( 0, 0,  1);
            east  = new Vector3( 1, 0,  0);
            west  = new Vector3(-1, 0,  0);
        }else if(angle == 90f) {
            north = new Vector3(-1, 0,  0);
            south = new Vector3( 1, 0,  0);
            east  = new Vector3( 0, 0, -1);
            west  = new Vector3( 0, 0,  1);
        }else if(angle == 180f) {
            north = new Vector3( 0, 0,  1);
            south = new Vector3( 0, 0, -1);
            east  = new Vector3(-1, 0,  0);
            west  = new Vector3( 1, 0,  0);
        }else if(angle == 270f) {
            north = new Vector3( 1, 0,  0);
            south = new Vector3(-1, 0,  0);
            east  = new Vector3( 0, 0,  1);
            west  = new Vector3( 0, 0, -1);
        }

//        north.scl(.5f);
//        south.scl(.5f);
//        east.scl(.5f);
//        west.scl(.5f);

        /*
        if(frameCount == 30)
        System.out.printf("Directions:\n" +
                "\tNorth:\n" +
                "\t\tX: %s\n" +
                "\t\tY: %s\n" +
                "\t\tZ: %s\n" +
                "\tSouth:\n" +
                "\t\tX: %s\n" +
                "\t\tY: %s\n" +
                "\t\tZ: %s\n" +
                "\tEast:\n" +
                "\t\tX: %s\n" +
                "\t\tY: %s\n" +
                "\t\tZ: %s\n" +
                "\tWest:\n" +
                "\t\tX: %s\n" +
                "\t\tY: %s\n" +
                "\t\tZ: %s\n",
                north.x, north.y, north.z,
                south.x, south.y, south.z,
                east.x,  east.y,  east.z,
                west.x,  west.y,  west.z);
                */
    }

    boolean wPressed = false, aPressed = false, sPressed = false, dPressed = false;

    void playerControls() {
        if(Gdx.input.isKeyPressed(Input.Keys.HOME)) {
            for(GameObject obj : AbstractScreen.instances)
                AbstractScreen.world.removeRigidBody(obj.body);
            AbstractScreen.networks.clear();
            game.setScreen(new MenuScreen(game));
        }
        wPressed = Gdx.input.isKeyPressed(Input.Keys.W);
        sPressed = Gdx.input.isKeyPressed(Input.Keys.S);
        aPressed = Gdx.input.isKeyPressed(Input.Keys.A);
        dPressed = Gdx.input.isKeyPressed(Input.Keys.D);
        if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {

//            playerInst.get(0).body.applyCentralForce(new Ve);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            if(jumpable) {
                jumping = true;
                float scalar = (float)Math.PI/2f;
                Vector3 jump = new Vector3(-gravity.x*scalar, -gravity.y*scalar, -gravity.z*scalar);
                playerInst.get(0).body.applyCentralImpulse(jump.scl(scalar/3));
                jumpable = false;
//                jumping = true;
//                inAir = false;
//                jumpValue = 3;
//                float scalar = (float)Math.PI*10;
//                Vector3 jump = new Vector3(-gravity.x*scalar, -gravity.y*scalar, -gravity.z*scalar);
////                playerInst.get(0).body.translate(jump);
//                jump.scl(scalar);
//                Vector3 linear = new Vector3(playerInst.get(0).body.getLinearVelocity());
//                playerInst.get(0).body.setLinearVelocity(linear.add(jump));
//                jumpable = false;
            }
        }
        if(wPressed)
            playerInst.get(0).applyGravity(north);
        if(sPressed)
            playerInst.get(0).applyGravity(south);
        if(aPressed)
            playerInst.get(0).applyGravity(west);
        if(dPressed)
            playerInst.get(0).applyGravity(east);

        if(!wPressed && !sPressed && !aPressed && !dPressed) {
//            GameObject obj = playerInst.get(0);
//            Vector3 lin = obj.body.getLinearVelocity(), ang = obj.body.getAngularVelocity();
//            obj.body.setLinearVelocity(new Vector3(lin.x/slowFactor, lin.y, lin.z/slowFactor).add(gravity));
//            obj.body.setAngularVelocity(new Vector3(ang.x/slowFactor, ang.y/slowFactor, ang.z/slowFactor).add(gravity));
        }
    }

    public void spawn() {
        GameObject obj;
        do {
            obj = constructors.values[MathUtils.random(constructors.size - 1)].construct();
        }while(obj.part.partId.toLowerCase().contains("ground") || obj.part.partId.toLowerCase().contains("wall") || obj.part.partId.toLowerCase().contains("player"));
        obj.transform.setFromEulerAngles(MathUtils.random(360f), MathUtils.random(360f), MathUtils.random(360f));
        obj.transform.trn(MathUtils.random(-2.5f, 2.5f), 9f, MathUtils.random(-2.5f, 2.5f));
        obj.body.proceedToTransform(obj.transform);
        obj.body.setUserValue(instances.size);
        obj.body.setCollisionFlags(obj.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
        instances.add(obj);
        world.addRigidBody(obj.body);
        obj.body.setContactCallbackFlag(Flags.OBJECT_FLAG);
        obj.body.setContactCallbackFilter(Flags.GROUND_FLAG);
    }

    float frameCount = 0;

    void updateCamera() {
        frameCount = (frameCount+1)%60;
        GameObject player = playerInst.get(0);
        Matrix4 playerPos = player.transform.cpy();
        /*
        if(frameCount == 0) {
            System.out.println(playerPos.toString());
            System.out.printf("Player Position: \n" +
                            "\tX: %s\n" +
                            "\tY: %s\n" +
                            "\tZ: %s\n",
                    playerPos.getValues()[Matrix4.M03], playerPos.getValues()[Matrix4.M13], playerPos.getValues()[Matrix4.M23]);
            for(int i = 0; i < playerPos.getValues().length; i++)
                System.out.println("\tIndex "+i+": "+playerPos.getValues()[i]);
        }
        */
        spriteBatch.begin();
        if(Main.isDebug) {
            font.draw(spriteBatch, "X: "+playerPos.getValues()[Matrix4.M03], 100, 440);
            font.draw(spriteBatch, "Y: "+playerPos.getValues()[Matrix4.M13], 100, 420);
            font.draw(spriteBatch, "Z: "+playerPos.getValues()[Matrix4.M23], 100, 400);
        }
        spriteBatch.end();
        cam.position.set(playerPos.getValues()[Matrix4.M03], playerPos.getValues()[Matrix4.M13], playerPos.getValues()[Matrix4.M23]);
        cam.update();
    }

    boolean checkCollision(btCollisionObject obj0, btCollisionObject obj1) {
        CollisionObjectWrapper co0 = new CollisionObjectWrapper(obj0);
        CollisionObjectWrapper co1 = new CollisionObjectWrapper(obj1);
        btCollisionAlgorithm algorithm = dispatcher.findAlgorithm(co0.wrapper, co1.wrapper);
        btDispatcherInfo info = new btDispatcherInfo();
        btManifoldResult result = new btManifoldResult(co0.wrapper, co1.wrapper);
        algorithm.processCollision(co0.wrapper, co1.wrapper, info, result);
        boolean r = result.getPersistentManifold().getNumContacts() > 0;
        dispatcher.freeCollisionAlgorithm(algorithm.getCPointer());
        result.dispose();
        info.dispose();
        co1.dispose();
        co0.dispose();
        return r;
    }

    @Override
    public void dispose() {
        for(GameObject obj : instances)
            obj.dispose();
        instances.clear();
        originalInstances.clear();
        for(GameObject.Constructor ctor : constructors.values())
            ctor.dispose();
        constructors.clear();
        dispatcher.dispose();
        networks.clear();
        cfg.dispose();
        batch.dispose();
        model.dispose();
        contactListener.dispose();
        world.dispose();
        constraintSolver.dispose();
        broadphase.dispose();
    }

    @Override public void resize(int width, int height) { cam.viewportWidth = width; cam.viewportHeight = height; }

    ArrayList<Material> mats;

    public void shadowPrepare() {
        mats = new ArrayList<>();
        for(GameObject obj : instances){
            try {
                mats.add(obj.materials.first().copy());
                obj.materials.first().remove(BlendingAttribute.Type);
            }catch(Exception e) {}
        }
    }
    public void shadowRecover() {
        int index = 0;
        for(GameObject obj : instances) {
            try {
                obj.materials.first().set(mats.get(index));
                index++;
            }catch(Exception e) {}
        }
    }

    public void show() {
        super.initWorld();
        super.initWorldCFG();
//        triggers.add(new StartTrigger("Start", new Vector6(9, 24, 9, 11, 26, 11)));
//        triggers.add(new EndTrigger("Ending", new Vector6(0, 0, 2, 9, 9, 12)));
//        triggers.add(new LaunchTrigger("Launcher", new Vector6(-15f, 0, -15f, -5f, 5, -5f)));
//        triggers.add(new DeathTrigger("Death", new Vector6(-15, -10, 0, -7.5f, -5, 15)));
//        triggers.add(new GravityTrigger("InverseGravity", new Vector6(13, 20, 13, 15, 25, 15)));
        activeTriggers = new Array<>(256);
        inactiveTriggers = new Array<>(256);
        PlayerObject pObjInst = new PlayerObject();
        playerInst.add(pObjInst.getObject());
        for(int i = 0; i < constructors.size; i++)
            originalInstances.add(constructors.getValueAt(i).construct());

        modelInstances.clear();
        tempInstances.clear();

        for(int tmp = 0; tmp < originalInstances.size; tmp++) {
            GameObject obj = originalInstances.get(tmp);
            if(obj.part.shape.toLowerCase().contains("custom")) {
                System.out.println("Obj.part.shape: "+obj.part.shape);
                String path = obj.part.shape.toLowerCase().replace("custom:", "");
                System.out.println("Path: "+path);
                path = "core/assets/models/"+path;
                System.out.println("Path: "+path);
                ModelInfo modelInfo = ModelBuilder.loadModel(path, obj.part);
                if(modelInfo != null) {
                    for(ModelInstanceData inst : modelInfo.instances)
                        modelInstances.add(inst.instance);
                    obj = modelInfo.constructor.construct();
                }
            }else if(obj.part.shape.toLowerCase().contains("prefab")) {
                System.out.println("Obj.part.shape: "+obj.part.shape);
                String path = obj.part.shape.toLowerCase().replace("prefab:", "");
                System.out.println("Path: "+path);
                path = "core/assets/models/"+path;
                System.out.println("Path: "+path);
//                PrefabHandler.BoxVertInfo box = new PrefabHandler.BoxVertInfo("");
                PrefabHandler.Loader loader = new PrefabHandler.Loader(new File(path));
                ArrayList<PrefabHandler.BoxVertInfo> boxList = loader.getBoxInfo();
                mf.begin();
                ModelFactory mf2 = new ModelFactory(new com.badlogic.gdx.graphics.g3d.utils.ModelBuilder());
                mf2.begin();
                int index = 0;
                Material mat = new Material();
                mat.set(new ColorAttribute(ColorAttribute.createDiffuse(obj.part.colour)));
                mat.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, obj.part.alpha/100));
                mat.set(new TextureAttribute(TextureAttribute.Diffuse, wallTex));
                for(PrefabHandler.BoxVertInfo boxInfo : boxList) {
                    Vector3[] tmpVerts = new Vector3[8];
                    for(int tmpIndex = 0; tmpIndex < 8; tmpIndex++) {
                        tmpVerts[tmpIndex] = new Vector3(boxInfo.getVerts()[tmpIndex]);
                        tmpVerts[tmpIndex].x *= obj.part.size.x;
                        tmpVerts[tmpIndex].y *= obj.part.size.y;
                        tmpVerts[tmpIndex].z *= obj.part.size.z;
                    }
                    mf.buildModelPart(index+": "+boxInfo.node, 4, mat)
                            .box(tmpVerts[0], tmpVerts[1], tmpVerts[2], tmpVerts[3],
                                    tmpVerts[4], tmpVerts[5], tmpVerts[6], tmpVerts[7]);
                    mf2.buildModelPart(index+": "+boxInfo.node, 4, mat)
                            .box(tmpVerts[0], tmpVerts[1], tmpVerts[2], tmpVerts[3],
                                    tmpVerts[4], tmpVerts[5], tmpVerts[6], tmpVerts[7]);
                    boxInfo.setNode("Prefab:"+obj.node).setModel(mf2.finishBuild()).setInstance(new ModelInstance(boxInfo.model));
                    boxInfo.getInstance().transform.setToTranslation(obj.part.pos);
                    prefabInstances.add(boxInfo);
                }
                Model model1 = mf.finishBuild();
                GameObject.Constructor boxBuilder = new GameObject.Constructor(model1, obj.node,
                        Bullet.obtainStaticNodeShape(model1.nodes), 0, obj.part);
                obj = boxBuilder.construct();
//                ModelInstance instance = new ModelInstance();
            }
            System.out.println(obj.type.toString().toLowerCase());
            switch (obj.type.toString().toLowerCase()) {
                case "static":
                    obj.body.setCollisionFlags(obj.body.getCollisionFlags() |
                            btCollisionObject.CollisionFlags.CF_STATIC_OBJECT);
                    break;
                case "kinematic":
                    obj.body.setCollisionFlags(obj.body.getCollisionFlags() |
                            btCollisionObject.CollisionFlags.CF_KINEMATIC_OBJECT);
                    break;
                case "dynamic":
                    obj.body.setCollisionFlags(obj.body.getCollisionFlags() |
                            btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
                    break;
                default:
                    obj.body.setCollisionFlags(obj.body.getCollisionFlags() |
                            btCollisionObject.CollisionFlags.CF_STATIC_OBJECT);
                    break;
            }
            obj.transform.setFromEulerAngles(obj.part.rot.x, obj.part.rot.y, obj.part.rot.z);
            obj.transform.trn(obj.part.pos);
            obj.body.setRollingFriction(.3f);
            obj.body.setFriction(.3f);
            obj.body.proceedToTransform(obj.transform);
            obj.body.setUserValue(tmp);
            instances.add(obj);
            obj.parent = instances;
            world.addRigidBody(obj.body);
            obj.body.setContactCallbackFlag(Flags.GROUND_FLAG);
            obj.body.setContactCallbackFilter(0);
            obj.body.setActivationState(Collision.DISABLE_DEACTIVATION);
        }

        TriggerArea spawnTrigger = null;
        int index = 0;
        try {
            while (spawnTrigger == null) {
                TriggerArea tmp = triggers.get(index);
                System.out.println("Checking trigger " + tmp.id + " at index " + index);
                if (tmp.type == EnumTriggers.START)
                    spawnTrigger = triggers.get(index);
                index++;
            }
            spawn = spawnTrigger.boundingBox.getOrigin();
        }catch(Exception e) {
            System.out.println("Spawn Trigger not found, reverting to fallback spawn.");
            spawn = new Vector3(0, 10, 0);
        }
//        cam.lookAt(spawn.add(0, 0, 10));

        GameObject playerObject = playerInst.get(0);
        playerObject.body.setCollisionFlags(btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
        playerObject.body.setUserValue(-1);
        playerObject.transform.setToTranslation(spawn);
        playerObject.body.setWorldTransform(playerObject.transform);
        playerObject.body.setRollingFriction(.3f);
        playerObject.body.setFriction(.3f);
        playerObject.body.setContactCallbackFlag(Flags.OBJECT_FLAG);
        playerObject.body.setContactCallbackFilter(Flags.GROUND_FLAG);
        playerObject.body.setActivationState(Collision.DISABLE_DEACTIVATION);
        world.addRigidBody(playerObject.body);

        endSprite = PixmapHandler.drawSolidSquare(0, 0, 0, 1);
        deathSprite = PixmapHandler.drawSolidSquare(0.76f, 0, 0, 0.86f);
        deathSprite.setBounds(-5, -5, Gdx.graphics.getWidth()+10, Gdx.graphics.getHeight()+10);

        time = new Timer();
        time.start();
        isEnd = false;
        gravity = defaultGravity;
        if(info.preview.type != null)
            switch (info.preview.type) {
                case PUZZLE:    changeTrack(MusicTracks.GAMEPUZZ); break;
                case TIME:      changeTrack(MusicTracks.GAMETIME); break;
                default:        changeTrack(MusicTracks.GAMEMISC);
            }
        else
            changeTrack(MusicTracks.GAMEMISC);

        cfg.obtain();
        dispatcher.obtain();
        broadphase.obtain();
        constraintSolver.obtain();
        world.obtain();
        drawer.obtain();
        contactListener.obtain();

        angles = new Float3();
        speeds = new Float3();

        for(Network n : info.networks)
            networks.add(n);
    }
}
