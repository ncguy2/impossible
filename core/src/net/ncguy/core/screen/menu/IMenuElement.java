package net.ncguy.core.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import net.ncguy.core.Main;
import net.ncguy.core.screen.MenuScreen;

import java.io.File;

/**
 * Created by ncguy on 15/11/2014 at 14:19.
 */
public interface IMenuElement {

    public static String rootLevelPath = "core/assets/levels/";
    public static String officialLevelPath = rootLevelPath+"official";
    public static String communityLevelPath = rootLevelPath+"community";

    public void onRender(float delta, MenuScreen game);
    public void onInit(MenuScreen game);

}
