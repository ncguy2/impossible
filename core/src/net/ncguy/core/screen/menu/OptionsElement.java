package net.ncguy.core.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisTextButton;
import net.ncguy.commonutil.Persist;
import net.ncguy.commonutil.screen.AbstractScreen;
import net.ncguy.commonutil.util.patch.Patch;
import net.ncguy.commonutil.util.file.PathResolution;
import net.ncguy.commonutil.util.patch.TextPos;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.core.util.MenuState;
import net.ncguy.utils.obj.Object2;

/**
 * Created by ncguy on 25/11/2014 at 12:15.
 */
public class OptionsElement extends BaseMenuElement {

    VisSlider musicVolumeSlider;

    @Override
    public void onRender(float delta, MenuScreen game) {
        alpha = 1;
        addSprite(bg);
        addPatch(patch1.setButton(false));
        addPatch(patch0.setButton(false));

//        addPatch(patch3.setButton(false));
//        addPatch(patch2.setButton(false));
//        patch3.size.x = (Persist.options.getMusicVolume()*3)+16;

//        addPatch(patch4.setButton());
//        addPatch(patch5.setButton());

        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) Persist.options.tweakMusicVolume(-.5f);
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) Persist.options.tweakMusicVolume(.5f);

        super.onRender(delta, game);

//        super.onRender(delta, game);
    }

    @Override
    public void onInit(MenuScreen game) {
        bg = new Sprite(PathResolution.getTex("core/assets/textures/shattered_ball.jpg"));

        String str = "Music volume";
        patch0 = new Patch(basePatch, 20, 575, 320, 90, Color.TEAL, str, TextPos.TOP);
        patch1 = new Patch(basePatch, 10, 10, 1260, 700, Color.BLUE, "Options", TextPos.TOP);
//
//        // Music
//        patch2 = new Patch(framePatch, 20, 550, 324, 50, new Color(0, 0.76f, 0, 1), "Music Volume");
//        patch3 = new Patch(fillPatch,  24, 554, 316, 42, Color.GREEN);
//
//        patch4 = new Patch(basePatch, 32, 505, 45, 45, Color.TEAL, "-");
//        patch5 = new Patch(basePatch, 96, 505, 45, 45, Color.TEAL, "+");
//
//        patch6 = new Patch(basePatch, 0, 0, 0, 0, Color.RED);

        super.onInit(game);

        VisTextButton returnBtn = new VisTextButton("Main Menu");
        returnBtn.setBounds(20, 20, 130, 45);
        returnBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Persist.options.saveFile();
                MenuScreen.changeState(MenuState.MAINMENU);
            }
        });
        stage.addActor(returnBtn);

        VisLabel musicVolumeText = new VisLabel(Persist.options.getMusicVolumeI()+"/100");
        musicVolumeText.setPosition(180-(musicVolumeText.getTextBounds().width/2), 580);
        stage.addActor(musicVolumeText);

        musicVolumeSlider = new VisSlider(0, 100, 1, false);
        musicVolumeSlider.setBounds(30, 600, 300, 50);
        musicVolumeSlider.setAnimateDuration(.1f);
        musicVolumeSlider.setValue(Persist.options.getMusicVolume());
        musicVolumeSlider.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Persist.options.setMusicVolume(musicVolumeSlider.getValue());
                musicVolumeText.setText(Persist.options.getMusicVolumeI()+"/100");
                musicVolumeText.setPosition(180-(musicVolumeText.getTextBounds().width/2), 580);
                MenuScreen.currentMusic.setVolume(Persist.options.getMusicVolume());
            }
        });
        stage.addActor(musicVolumeSlider);
        isUsingStage = true;


    }

}
