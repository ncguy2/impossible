package net.ncguy.core.screen.menu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.widget.*;
import net.ncguy.commonutil.screen.AbstractScreen;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.core.util.MenuState;

import java.util.*;

/**
 * Created by Nick on 10/01/2015 at 15:51.
 * Project: Development
 * Package: net.ncguy.core.screen.menu
 */
public class LobbyElement extends BaseMenuElement {

    public java.util.List<String> clients = new ArrayList<>();
    public java.util.List<String> messages = new ArrayList<>();

    @Override
    public void onRender(float delta, MenuScreen game) {

        clientContainer.clear();
        for(String c : clients) {
            VisLabel l = new VisLabel(c);
            clientContainer.add(l);
            clientContainer.row();
        }

        super.onRender(delta, game);
    }

    @Override
    public void onInit(MenuScreen game) {
        clients.clear();

        super.onInit(game);
        isUsingStage = true;
        clientContainer = new VisTable();
        clientContainer.clear();
        for(String c : clients) {
            VisLabel l = new VisLabel(c);
            clientContainer.add(l);
            clientContainer.row();
        }

        clientPane = new VisScrollPane(clientContainer);
        clientPane.setBounds(20, 120, 120, 256);
        stage.addActor(clientPane);

        messageArea = new VisTextArea("");
        messageArea.setBounds(150, 120, 256, 256);
        stage.addActor(messageArea);
        for(String s : messages) {
            messageArea.setText(messageArea.getText()+s);
        }

        messageBox = new VisTextField("");
        messageBox.setBounds(150, 70, 256, 45);
        stage.addActor(messageBox);

        sendMessage = new VisTextButton("Send");
        sendMessage.setBounds(420, 70, 60, 45);
        sendMessage.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                String str = "";
                if(messageBox.getText().length() > 0)
                    str = userField.getText()+": "+messageBox.getText()+"\n";
                else
                    return;
                messages.add(str);
                messageArea.setText("");
                for(String s : messages) {
                    messageArea.setText(messageArea.getText()+s);
                }
                messageBox.setText("");
            }
        });
        stage.addActor(sendMessage);

        userField = new VisTextField("");
        userField.setBounds(150, 385, 256, 45);
        userField.setText("User 1");
        stage.addActor(userField);

        VisTextButton returnBtn = new VisTextButton("Return");
        returnBtn.setBounds(10, AbstractScreen.h-55, 130, 45);
        returnBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.MAINMENU);
            }
        });
        stage.addActor(returnBtn);
    }

    VisTextButton connectBtn, sendMessage;
    VisTextField messageBox, ipField, userField;
    VisTextArea messageArea;
    VisTable clientContainer, messageContainer;
    VisScrollPane clientPane, textPane;

}
