package net.ncguy.core.screen.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.widget.VisTextButton;
import net.ncguy.commonutil.factory.GameObject;
import net.ncguy.commonutil.screen.AbstractScreen;
import net.ncguy.commonutil.util.patch.Patch;
import net.ncguy.commonutil.util.file.PathResolution;
import net.ncguy.core.screen.GameScreen;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.core.util.MenuState;

/**
 * Created by ncguy on 18/11/2014 at 11:40.
 */
public class EndLevelElement extends BaseMenuElement {

    private float speed = 1.5f;
    public float buttonAlpha = 0, titleAlpha = 0, bgAlpha = 0;
    public boolean buttonLoaded = false, titleLoaded = false, bgLoaded = false;

    @Override
    public void onRender(float delta, MenuScreen game) {
        sprite.setColor(sprite.getColor().r, sprite.getColor().g, sprite.getColor().b, bgAlpha);
        test.setColor(test.getColor().r, test.getColor().g, test.getColor().b, titleAlpha);
        patch1.colour  = new Color(patch1.colour.r,  patch1.colour.g,  patch1.colour.b,  buttonAlpha);
        patch2.colour = new Color(patch2.colour.r, patch2.colour.g, patch2.colour.b, buttonAlpha);
        patch3.colour = new Color(patch3.colour.r, patch3.colour.g, patch3.colour.b, buttonAlpha);

        addSprite(sprite);
        addSprite(test);
        if(buttonLoaded) {
            addPatch(patch1.setButton());
            addPatch(patch2.setButton());
            addPatch(patch3.setButton());
        }else{
            addPatch(patch1);
            addPatch(patch2);
            addPatch(patch3);
        }

        if(!bgLoaded) {
            bgAlpha += 1/(speed*60);
            if(bgAlpha >= 1) { bgLoaded = true; bgAlpha = 1; }
        }
        if(bgLoaded && !titleLoaded) {
            titleAlpha += 1/(speed*60);
            if(titleAlpha >= 1) { titleLoaded = true; titleAlpha = 1; }
        }
        if(titleLoaded && !buttonLoaded) {
            buttonAlpha += 1/(speed*60);
            if(buttonAlpha >= 1) { buttonLoaded = true; buttonAlpha = 1; }
        }

//        addText("bg: "+bgAlpha, 100, 160);
//        addText("title: "+titleAlpha, 100, 140);
//        addText("button: "+buttonAlpha, 100, 120);
        if(!buttonLoaded) {
            sceneBatch.setColor(.5f, 0, .5f, buttonAlpha);
            retryBtn.setColor(1, 1, 1, buttonAlpha);
            menuBtn.setColor(1, 0, 0, buttonAlpha);
            lvlBtn.setColor(0, .9f, 0, buttonAlpha);
        }else {
            sceneBatch.setColor(1, 1, 1, 1);
            retryBtn.setColor(1, 1, 1, 1);
            menuBtn.setColor(1, 0, 0, 1);
            lvlBtn.setColor(0, 0.9f, 0, 1);
        }
        super.onRender(delta, game);
    }

    @Override
    public void onInit(MenuScreen game) {
        System.out.println("EndLevelElement.onInit");
        buttonAlpha  = titleAlpha  = bgAlpha  = 0f;
        buttonLoaded = titleLoaded = bgLoaded = false;

        sprite = new Sprite(PathResolution.getTex("core/assets/textures/wall.png"));
        test = new Sprite(PathResolution.getTex("core/assets/textures/complete.png"));
        patch1 = new Patch(basePatch, (int)(AbstractScreen.w/2)-65,       (int)(AbstractScreen.h/2)-22, 130, 45, Color.PURPLE, "Retry");
        patch2 = new Patch(basePatch, (int)((AbstractScreen.w/2)-65)-150, (int)(AbstractScreen.h/2)-22, 130, 45, Color.RED, "Menu");
        patch3 = new Patch(basePatch, (int)((AbstractScreen.w/2)-65)+150, (int)(AbstractScreen.h/2)-22, 130, 45, new Color(0, 0.9f, 0, 1), "Level Select");

        super.onInit(game);

        retryBtn = new VisTextButton("Retry");
        retryBtn.setSize(150, 45);
        retryBtn.setBounds((int)(AbstractScreen.w/2)-65, (int)(AbstractScreen.h/2)-22, 130, 45);
        retryBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.game.setScreen(new GameScreen(MenuScreen.game));
            }
        });
        stage.addActor(retryBtn);
        menuBtn = new VisTextButton("Menu");
        menuBtn.setBounds((int)((AbstractScreen.w/2)-65)-150, (int)(AbstractScreen.h/2)-22, 130, 45);
        menuBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.MAINMENU);
            }
        });
        stage.addActor(menuBtn);
        lvlBtn = new VisTextButton("Level Select");
        lvlBtn.setBounds((int)((AbstractScreen.w/2)-65)+150, (int)(AbstractScreen.h/2)-22, 130, 45);
        lvlBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.LEVELOFFICIAL);
            }
        });
        stage.addActor(lvlBtn);

        isUsingStage = true;
        for(GameObject obj : AbstractScreen.instances)
            AbstractScreen.world.removeRigidBody(obj.body);
        AbstractScreen.networks.clear();
    }

    VisTextButton retryBtn, menuBtn, lvlBtn;

}
