package net.ncguy.core.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import net.ncguy.commonutil.handlers.LevelLoader;
import net.ncguy.commonutil.screen.AbstractScreen;
import net.ncguy.commonutil.skin.LabelTypewriter;
import net.ncguy.commonutil.skin.SpriteActor;
import net.ncguy.commonutil.util.LevelPreview;
import net.ncguy.commonutil.util.LevelType;
import net.ncguy.commonutil.util.patch.Patch;
import net.ncguy.commonutil.util.file.PathResolution;
import net.ncguy.commonutil.util.patch.TextPos;
import net.ncguy.core.Main;
import net.ncguy.core.screen.GameScreen;
import net.ncguy.core.screen.LoadingScreen;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.core.util.MenuState;
import net.ncguy.utils.general.TextUtils;

import java.io.File;

/**
 * Created by ncguy on 15/11/2014 at 14:28.
 */
public class LevelSelectOElement extends BaseMenuElement {

    public static Array<LevelPreview> levels = new Array<>();
    public float originX = 450;
    public Sprite square;
    public static int index = 0, selectedIndex = 0;
    public static boolean nextToggle = true, prevToggle = true;
    public float nextDelta, prevDelta;
    public float slideIndex;
    public int unlockedIndex = 250;
    public float speed = 10f;
    public LevelType currentType, previousType;
    public LevelPreview selectedLevel;

    public LevelSelectOElement() {}

    @Override
    public void onRender(float delta, MenuScreen game) {

        speed = 12.5f;

        originX = ((MenuScreen.w/2)-64)-(150*selectedIndex);
        if(slideIndex > originX) {
            slideIndex -= (slideIndex-originX)/speed;
        }else if(slideIndex < originX) {
            slideIndex += (originX-slideIndex)/speed;
        }

        index = 0;

//        processToggleState(delta);

//        patch3.text = levels.get(selectedIndex).name+"";

//        changeType(levels.get(selectedIndex).type);
//        if(currentType != previousType)
//            test.set(currentType.getSprite());

        patch0.setText(selectedLevel == null ? "No Level Selected" : selectedLevel.name);

        addSprite(bg);
        addPatch(patch0.setButton(false));
        addPatch(patch1.setButton(false));
        addSprite2(selectedLevel.type.getSprite());
        filePathLbl.update(delta);
//        addPatch(patch2.setButton());
//        addPatch(patch3.setButton(false));
//        if(selectedIndex > unlockedIndex) addPatch(patch4.setButton(false));
//        else addPatch(patch4.setButton());
//        addPatch(patch5.setButton());
//        addPatch(patch6.setButton());
//        addPatch(patch7.setButton());
//        addPatch(patch8.setButton());
//        addText("Current State: "+game.currentState.name(), 100, 100);
//        addText("File Path: "+levels.get(selectedIndex).path, 10, (int)MenuScreen.h-10);
//        addSprite(test);
//        addSprite(sprite);

        super.onRender(delta, game);
    }

    @Override
    public void onInit(MenuScreen game) {
        Pixmap map = new Pixmap(256, 256, Pixmap.Format.RGBA8888);
        map.setColor(1, 1, 0, 1);
        map.fillTriangle(0, 0, 0, 256, 256, 256);
        Texture tex = new Texture(map);
        map.dispose();
        sprite = new Sprite(tex);
        map = new Pixmap(128, 64, Pixmap.Format.RGBA8888);
        map.setColor(0.46f, 0.74f, 1, 1);
        map.fillRectangle(0, 0, 128, 64);
        tex = new Texture(map);
        map.dispose();
        square = new Sprite(tex);
        square.setY(160);
        bg = new Sprite(new Texture("core/assets/textures/lvlOBg.png"));
        bg.setX(0);
        bg.setY(0);
        map = new Pixmap(128, 64, Pixmap.Format.RGBA8888);
        map.setColor(0.16f, 0.44f, 0.7f, 1);
        map.fillRectangle(0, 0, 128, 64);
        tex = new Texture(map);
        map.dispose();
        image = new Sprite(tex);
        image.setY(160);

//        test = new Sprite(LevelType.UNKNOWN.getSprite());
//        test.setX(64);
//        test.setY(256);

        levels.clear();
        File levelPath = new File(IMenuElement.officialLevelPath);
        File[] levelList = levelPath.listFiles();
        for(File a : levelList) {
            System.out.println(a.getName()+": "+a.getAbsolutePath());
            LevelPreview lp = LevelLoader.getLevelData(a);
            levels.add(lp);
        }

        patch0 = new Patch(basePatch, 10, 110, (int)AbstractScreen.w-20, (int)AbstractScreen.h-120, Color.TEAL, "No Level Selected", TextPos.TOP);

        patch1 = new Patch(bracePatch, 130, 250, 320, 320, Color.WHITE);
//        patch2 = new Patch(basePatch, (int)AbstractScreen.w-(130+10), 105, 130, 45, Color.RED, "Next");

//        patch3 = new Patch(basePatch, (int)(AbstractScreen.w/2)-65, 100, 130, 45, Color.PURPLE, "");
//        patch4 = new Patch(basePatch, (int)(AbstractScreen.w/2)-65, 50, 130, 45, Color.ORANGE, "Play");

//        patch5 = new Patch(basePatch, 10, 55, 130, 45, Color.MAROON, "Previous(5)");
//        patch6 = new Patch(basePatch, (int)AbstractScreen.w-(130+10), 55, 130, 45, Color.RED, "Next(5)");

//        patch7 = new Patch(basePatch, 10, 5, 130, 45, Color.MAROON, "First");
//        patch8 = new Patch(basePatch, (int)AbstractScreen.w-(130+10), 5, 130, 45, Color.RED, "Last");

        super.onInit(game);

        VisTable table = new VisTable(true);
        VisScrollPane pane = new VisScrollPane(table);
        pane.setBounds(10, 10, Gdx.graphics.getWidth()-20, 90);

        table.pad(10).defaults().expandX().space(4);
        table.defaults().setActorWidth(150);
        table.defaults().setActorHeight(50);
        for(LevelPreview lp : levels) {
            if(selectedLevel == null) selectedLevel = lp;
            VisTextButton btn = new VisTextButton(lp.name);
            btn.setWidth(130);
            btn.setHeight(45);

            if(lp.name.length() <= 0) {
                btn.setText(TextUtils.getFileName(new File(lp.path)));
            }
            btn.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    changeSelectedLevel(lp);
                }
            });
            table.add(btn).expand(130, 45);
        }
        stage.addActor(pane);

        VisTextButton returnBtn = new VisTextButton("Main Menu");
        returnBtn.setBounds(20, AbstractScreen.h-65, 130, 45);
        returnBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.MAINMENU);
            }
        });
        stage.addActor(returnBtn);

        VisTextButton playBtn = new VisTextButton("Play");
        playBtn.setBounds(AbstractScreen.w-150, 120, 130, 45);
        playBtn.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.game.setScreen(new GameScreen(MenuScreen.game));
            }
        });
        stage.addActor(playBtn);


        filePathLbl = new LabelTypewriter(new VisLabel("File Path: "));
        filePathLbl.label.setPosition(AbstractScreen.w-230, AbstractScreen.h-150);
        stage.addActor(filePathLbl.label);

            /*

            game.spriteBatch.begin();
        for(LevelPreview lp : levels) {
            square.setX(slideIndex+(150*index));
            image.setX(slideIndex+(150*index));

            square.draw(game.spriteBatch, alpha);
            if(index > unlockedIndex)
                image.draw(game.spriteBatch, alpha);
            index++;
        }
        game.spriteBatch.end();

             */

    }

    private LabelTypewriter filePathLbl;

    private void changeSelectedLevel(LevelPreview lp) {
        Main.level = lp.path;
        selectedLevel = lp;
        filePathLbl.setTargetText("File Name: ["+selectedLevel.getFileName()+"]");
    }

    private void processToggleState(float delta) {
        if(!nextToggle) {
            nextDelta -= delta;
            if(nextDelta <= 0) {
                nextToggle = true;
                nextDelta = 0.2f;
            }
        }
        if(!prevToggle) {
            prevDelta -= delta;
            if(prevDelta <= 0) {
                prevToggle = true;
                prevDelta = 0.2f;
            }
        }
    }

    private void changeType(LevelType newType) {
        if(currentType != null)
            previousType = currentType;
        currentType = newType;
    }

}
