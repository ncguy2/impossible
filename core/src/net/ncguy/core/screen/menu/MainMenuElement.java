package net.ncguy.core.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTextButton;
import net.ncguy.commonutil.Persist;
import net.ncguy.commonutil.util.patch.Patch;
import net.ncguy.commonutil.util.Tracker;
import net.ncguy.commonutil.util.file.PathResolution;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.core.util.MenuState;

import java.awt.*;

/**
 * Created by ncguy on 15/11/2014 at 14:20.
 */
public class MainMenuElement extends BaseMenuElement {

    public VisTextButton exitBtn, playBtn, optBtn, helpBtn, onlineBtn;

    public MainMenuElement() {
        VisUI.load();
    }

    @Override
    public void onRender(float delta, MenuScreen game) {
//        addPatch(patch0.setButton());
//        addPatch(patch1.setButton());
//        addPatch(patch2.setButton());
//        patch3.size.x = 130;
//        addPatch(patch3.setButton());
        addSprite(bg);
        addSprite(sprite);
        test.setX(Tracker.getPoint().x);
        test.setY(Tracker.getPoint().y);
        addSprite(test);
        addText("Current State: "+game.currentState.name(), 100, 100, Color.GREEN);

        super.onRender(delta, game);
    }

    @Override
    public void onInit(MenuScreen game) {
        Pixmap map = new Pixmap(4, 4, Pixmap.Format.RGBA8888);
        map.setColor(1, 0, 1, 1);
        map.fillTriangle(0, 0, 4, 0, 4, 4);
        Texture tex = new Texture(map);
        map.dispose();
        test = new Sprite(tex);
        bg = new Sprite(PathResolution.getTex("core/assets/textures/shattered_ball.jpg"));
        sprite = new Sprite(PathResolution.getTex("core/assets/textures/title.png"));
        sprite.setX(24);
        sprite.setY(460);

//        patch0 = new Patch(basePatch, 100, 180, 130, 45, Color.RED, "Exit");
//        patch1 = new Patch(basePatch, 100, 125, 130, 45, Color.MAGENTA, "Play Game");
//        patch2 = new Patch(basePatch, 250, 180, 130, 45, Color.MAGENTA, "Options");
//        patch3 = new Patch(basePatch, 250, 125, 130, 45, Color.MAGENTA, "Help");

        super.onInit(game);

        exitBtn     = new VisTextButton("Exit");
        playBtn     = new VisTextButton("Play");
        optBtn      = new VisTextButton("Options");
        helpBtn     = new VisTextButton("Help");
        onlineBtn   = new VisTextButton("Online");

        exitBtn.setColor(1, .4f, .4f, 1);

        exitBtn.setBounds(100, 180, 130, 45);
        playBtn.setBounds(100, 125, 130, 45);
        optBtn.setBounds(250, 180, 130, 45);
        helpBtn.setBounds(250, 125, 130, 45);
        onlineBtn.setBounds(100, 70, 280, 45);

        exitBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Persist.options.saveFile();
                Gdx.app.exit();
            }
        });
        playBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.LEVELOFFICIAL);
            }
        });
        optBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.MENUOPTIONS);
            }
        });
        helpBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.MENUHELP);
            }
        });
        onlineBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MenuScreen.changeState(MenuState.LOBBY);
            }
        });

        stage.addActor(exitBtn);
        stage.addActor(playBtn);
        stage.addActor(optBtn);
        stage.addActor(helpBtn);
        stage.addActor(onlineBtn);

    }

}
