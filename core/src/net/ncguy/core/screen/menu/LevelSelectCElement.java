package net.ncguy.core.screen.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import net.ncguy.commonutil.util.TextInfo;
import net.ncguy.core.screen.MenuScreen;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ncguy on 15/11/2014 at 14:28.
 */
public class LevelSelectCElement extends BaseMenuElement {

    public LevelSelectCElement() {}

    @Override
    public void onRender(float delta, MenuScreen game) {

        addText("Current State: "+game.currentState.name(), 100, 100);
        addSprite(sprite);

        super.onRender(delta, game);
    }

    @Override
    public void onInit(MenuScreen game) {
        Pixmap map = new Pixmap(256, 256, Pixmap.Format.RGBA8888);
        map.setColor(1, 1, 0, 1);
        map.fillTriangle(0, 0, 0, 256, 256, 256);
        Texture tex = new Texture(map);
        map.dispose();
        sprite = new Sprite(tex);
    }

}
