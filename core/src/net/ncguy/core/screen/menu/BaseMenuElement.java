package net.ncguy.core.screen.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import net.ncguy.commonutil.util.patch.Patch;
import net.ncguy.commonutil.util.TextInfo;
import net.ncguy.commonutil.util.Tracker;
import net.ncguy.commonutil.util.file.PathResolution;
import net.ncguy.core.screen.MenuScreen;
import net.ncguy.commonutil.mail.MailInit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ncguy on 15/11/2014 at 16:02.
 */
public class BaseMenuElement implements IMenuElement {

    public Stage stage;
    public SpriteBatch sceneBatch;
    public static boolean isUsingStage = false;

    public NinePatch basePatch  = new NinePatch(PathResolution.getTex("core/assets/textures/patch/basePatch.9.png"),  8, 8, 8, 8);
    public NinePatch framePatch = new NinePatch(PathResolution.getTex("core/assets/textures/patch/framePatch.9.png"), 8, 8, 8, 8);
    public NinePatch fillPatch  = new NinePatch(PathResolution.getTex("core/assets/textures/patch/fillPatch.9.png"),  8, 8, 8, 8);
    public NinePatch bracePatch = new NinePatch(PathResolution.getTex("core/assets/textures/patch/bracePatch.9.png"), 8, 8, 8, 8);
    public Sprite sprite, bg, test, image;
    public static Patch patch0, patch1, patch2, patch3, patch4, patch5, patch6, patch7, patch8, patch9;
    public float alpha;
    public List<TextInfo> textInfoList = new ArrayList<>();
    public List<Sprite> spriteList = new ArrayList<>(), spriteList2 = new ArrayList<>();
    public List<Patch> patchList = new ArrayList<>();

    public void addText(String text, int x, int y, Color colour) {
        textInfoList.add(new TextInfo(text, x, y));
    }
    public void addText(String text, int x, int y) {
        addText(text, x, y, Color.WHITE);
    }
    public void addSprite(Sprite sprite) {
        spriteList.add(sprite);
    }
    public void addSprite2(Sprite sprite) { spriteList2.add(sprite); }
    public void addPatch(Patch patch) {
        patchList.add(patch);
    }

    private void process(float delta, MenuScreen game) {
        if(game.currentElement != this) alpha = game.previousAlpha; else alpha = 1;

        game.spriteBatch.begin();
        for(Sprite s : spriteList) {
            if(s != null && game.spriteBatch != null) {
                try {
                    s.draw(game.spriteBatch, alpha);
                }catch(Exception e) {
                    MailInit.sendMail("ImPossible Issue: Sprite Error "+e.getCause(), e.getMessage());
                    System.out.println("BaseMenuElement.process >> " + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
        }

        for(Patch p : patchList) {
            if(p != null) {
                try {
                    if (p.isButton)
                        if (Tracker.hoverOverRegion(p.pos, p.size))
                            p.patch.setColor(new Color(p.colour).add(0.1f, 0.1f, 0.1f, 1));
                        else p.patch.setColor(p.colour);
                    else p.patch.setColor(p.colour);
                    p.patch.draw(game.spriteBatch, p.pos.x, p.pos.y, p.size.x, p.size.y);
                    if (p.text != "") {
                        Vector2 pos = new Vector2();
                        float x, y;
                        x = (p.pos.x);
                        y = (p.pos.y);
                        BitmapFont.TextBounds bounds = game.font.getBounds(p.text);
                        if (p.textPos.getPos().x == 0) {
                            x += 5;
                        } else if (p.textPos.getPos().x == 1) {
                            x += (p.size.x) / 2;
                            x -= bounds.width / 2f;
                        } else if (p.textPos.getPos().x == 2) {
                            x += (p.size.x);
                            x -= bounds.width;
                        }
                        if (p.textPos.getPos().y == 0) {
                            y += 5;
                        } else if (p.textPos.getPos().y == 1) {
                            y += (p.size.y) / 2;
                            y += bounds.height / 2f;
                        } else if (p.textPos.getPos().y == 2) {
                            y += (p.size.y);
                            y -= bounds.height;
                        }
                        pos.set(x, y);
                        game.font.setColor(1, 1, 1, p.colour.a);
                        game.font.draw(game.spriteBatch, p.text, pos.x, pos.y);
                        game.font.setColor(1, 1, 1, 1);
                    }
                }catch (Exception e) {
                    MailInit.sendMail("ImPossible Issue: Patch Error " + e.getCause(), e.getMessage());
                }
            }
        }

        for(Sprite s : spriteList2) {
            if(s != null && game.spriteBatch != null) {
                try {
                    s.draw(game.spriteBatch, alpha);
                }catch(Exception e) {
                    MailInit.sendMail("ImPossible Issue: Sprite Error "+e.getCause(), e.getMessage());
                    System.out.println("BaseMenuElement.process >> " + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
        }

        for(TextInfo t : textInfoList) {
            game.font.setColor(t.colour.r, t.colour.g, t.colour.b, alpha);
            game.font.draw(game.spriteBatch, t.text, t.pos.x, t.pos.y);
        }
        game.font.setColor(1, 1, 1, 1);
        textInfoList.clear();
        spriteList.clear();
        spriteList2.clear();
        patchList.clear();
        game.spriteBatch.end();

        try {
            if (isUsingStage) {
                sceneBatch.begin();
                stage.draw();
                stage.act(delta);
                sceneBatch.end();
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRender(float delta, MenuScreen game) {
        process(delta, game);
    }

    @Override
    public void onInit(MenuScreen game) {
        stage = new Stage();
        sceneBatch = new SpriteBatch();
        Gdx.input.setInputProcessor(stage);
        isUsingStage = true;
    }

}
