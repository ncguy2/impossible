package net.ncguy.core.player;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import net.ncguy.commonutil.screen.AbstractScreen;

/**
 * Created by 354584 on 26/09/2014.
 */
public abstract class Movement {

    private static Vector2 tmpv2 = new Vector2();
    static final Vector3 tmp = new Vector3();
    Vector3 position = new Vector3();
    Vector3 destination = new Vector3();
    boolean hasDest;
    Vector3 velocity = new Vector3();
    Vector3 acceleration = new Vector3();
    float accelRate = 0.01f;
    float maxHorizontalSpeed = 0.1f;
    float maxVerticalSpeed = 0.1f;
    float braking = 0.98f;
    boolean gravity = false;
    protected float arrivalThreshold = 5f;

    public Movement() {
    }

    public void update(float timeStep, boolean onGround) {
        if (onGround)
            if (velocity.y < 0f) velocity.y = 0f;
        if (!hasDest) {
            if (onGround) velocity.scl(braking);
        } else {
            accelerate(timeStep);
        }
        applyGravity(onGround);
        applyVelocity(timeStep, true);
    }

    abstract void accelerate(float timeStep);

    void limitVelocity() {
        velocity.y = MathUtils.clamp(velocity.y, -maxVerticalSpeed, maxVerticalSpeed);
        tmpv2.set(velocity.x, velocity.y);
        tmpv2.limit(maxHorizontalSpeed);
        velocity.x = tmpv2.x;
        velocity.y = tmpv2.y;
    }

    public void applyVelocity(float timeStep, boolean limit) {
        limitVelocity();
        tmp.set(velocity).scl(timeStep);
        position.add(tmp);
    }

    public void applyGravity(boolean onGround){
        if(!gravity) return;
        tmp.set(AbstractScreen.gravity);
        if(!onGround) {
            velocity.add(tmp);
        }
    }

    public float getBraking() { return braking; }
    public void setBraking(float braking) { this.braking = braking; }
    public Vector3 getVelocity() { return velocity; }
    public Vector3 getAcceleration() { return acceleration; }
    public float getAccelRate() { return accelRate; }
    public void setAccelRate(float accelRate) { this.accelRate = accelRate; }
    public float getMaxHorizontalSpeed() { return maxHorizontalSpeed; }
    public void setMaxHorizontalSpeed(float max) { this.maxHorizontalSpeed = max; }
    public float getMaxVerticalSpeed() { return maxVerticalSpeed; }
    public void setMaxVerticalSpeed(float max) { this.maxVerticalSpeed = max; }
    public Vector3 getDestination() { return destination; }
    public boolean hasDestination() { return hasDest; }
    public boolean isAffectedByGravity() { return gravity; }
    public void setAffectedByGravity(boolean gravity) { this.gravity = gravity; }
    public Vector3 getPosition() { return position; }
    public void setPosition(Vector3 pos) { this.position = pos; }

    public void setDestination(Vector3 newDest) {
        if(newDest == null)
            hasDest = false;
        else {
            hasDest = true;
            destination.set(newDest);
        }
    }
    public void cancelDestinationAtThreshold(float range) {
        if(hasDest) {
            if(destination.dst2(position) <= range * range) {
                hasDest = false;
            }
        }
    }
}