package net.ncguy.core.player;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by 354584 on 26/09/2014.
 */
public class GroundMovement extends Movement {

    public GroundMovement() { super(); }

    @Override
    void accelerate(float timeStep) {
        Vector3 diff = tmp;
        diff.set(destination).sub(position);
        diff.y = 0f;
        acceleration.set(diff).nor().scl(accelRate).scl(timeStep);
        velocity.add(acceleration);
    }
}
