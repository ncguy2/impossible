package net.ncguy.core.player;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btCylinderShape;
import net.ncguy.commonutil.factory.ModelFactory;
import net.ncguy.commonutil.handlers.LevelTranslation;
import net.ncguy.commonutil.handlers.ObjectType;
import net.ncguy.commonutil.util.editor.Enums;

/**
 * Created by ncguy2 on 26/09/2014.
 */
public class Player {

    public Movement movement;

    public Player() {}

    public static Model buildPlayerModel() {
        ModelBuilder mb = new ModelBuilder();
        ModelFactory mf = new ModelFactory(mb);
        Material mat = new Material();
//        mat.set(new ColorAttribute(ColorAttribute.createDiffuse(Color.WHITE)));
        mat.set(new ColorAttribute(ColorAttribute.createDiffuse(Color.WHITE)));
        mat.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 0));
        mf.buildModelPart("Player", 4, mat).sphere(2.5f, 2.5f, 2.5f, 50, 50);
        return mf.finishBuild();
    }

    public static LevelTranslation.TranslatedPart getPlayerPart() {
        LevelTranslation.TranslatedPart parts = new LevelTranslation.TranslatedPart();
        btCylinderShape cyl = new com.badlogic.gdx.physics.bullet.collision.btCylinderShape(new Vector3(2.5f/2f, (3.5f/2f)+(2f/2f), 2.5f/2f));

        parts.colour = Color.WHITE;
        parts.mass = 1f;
        parts.objShape = cyl;
        parts.partId = "player";
        parts.pos = Vector3.Zero;
        parts.rot = Vector3.Zero;
        parts.size = new Vector3(2.5f, 2.5f, 2.5f);
        parts.type = ObjectType.KINEMATIC;
        parts.shape = "sphere";
        parts.eShape = Enums.PartShape.SPHERE;
        return parts;
    }
}
