package net.ncguy.core.player;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by 354584 on 26/09/2014.
 */
public class AirMovement extends Movement {
    private static Vector2 tmpv2 = new Vector2();

    public AirMovement() { super(); }

    @Override
    void accelerate(float timeStep) {
        Vector3 diff = tmp;
        diff.set(destination).sub(position);
        acceleration.set(diff).nor().scl(accelRate).scl(timeStep);
        velocity.add(acceleration);
    }
}
