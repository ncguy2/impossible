package net.ncguy.core.player;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import net.ncguy.commonutil.factory.GameObject;
import net.ncguy.commonutil.handlers.LevelTranslation;
import net.ncguy.commonutil.handlers.ObjectType;
import net.ncguy.commonutil.util.Flags;
import net.ncguy.commonutil.util.editor.Enums;

/**
 * Created by ncguy on 07/10/2014 at 14:11.
 */
public class PlayerObject {

    GameObject playerObj, scanObj;

    public PlayerObject() {}

    public GameObject getObject() {
        if(this.playerObj != null)
            return this.playerObj;
        LevelTranslation.TranslatedPart part = new LevelTranslation.TranslatedPart();
        part.shape = "sphere";
        part.eShape = Enums.PartShape.SPHERE;
        part.partId = "Player";
        part.objShape = new btSphereShape(1.25f);
        part.colour = Color.WHITE;
        part.size = new Vector3(2.5f, 2.5f, 2.5f);
        part.alpha = 1f;
        part.pos = new Vector3(0, 0, 0);
        part.rot = Vector3.Zero;
        part.type = ObjectType.KINEMATIC;
        part.mass = 1;
        GameObject.Constructor build = new GameObject.Constructor(Player.buildPlayerModel(), "Player", part.objShape, 1, part);
        GameObject obj = build.construct();
//        obj.body.setCollisionFlags(btCollisionObject.CollisionFlags.CF_KINEMATIC_OBJECT);
        obj.body.setCollisionFlags(btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
        obj.transform.setFromEulerAngles(obj.part.rot.z, obj.part.rot.y, obj.part.rot.x);
        obj.transform.trn(obj.part.pos.x, obj.part.pos.y, obj.part.pos.z);
        obj.body.proceedToTransform(obj.transform);
        obj.body.setContactCallbackFlag(Flags.OBJECT_FLAG);
        obj.body.setContactCallbackFilter(Flags.GROUND_FLAG);
        this.playerObj = obj;
        return obj;
    }
}
