package net.ncguy.core.network;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.net.*;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;
import java.util.List;

/**
 * Created by Nick on 10/01/2015 at 16:03.
 * Project: Development
 * Package: net.ncguy.core.network
 */
public class Test implements ApplicationListener {

    private OrthographicCamera cam;
    private SpriteBatch batch;
    private Skin skin;
    private Stage stage;
    private Label labelDetails, labelMessage;
    private TextButton button;
    private TextArea textIpAddress, textMessage;

    public static final float V_WIDTH  = 1280;
    public static final float V_HEIGHT = 720;

    @Override
    public void create() {
        cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch = new SpriteBatch();
        skin = new Skin(Gdx.files.internal("core/assets/skin/uiskin.json"));
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        List<String> addresses = new ArrayList<>();
        try{
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            for(NetworkInterface ni : Collections.list(interfaces)) {
                for(InetAddress address : Collections.list(ni.getInetAddresses())) {
                    if(address instanceof Inet4Address) {
                        addresses.add(address.getHostAddress());
                    }
                }
            }
        }catch(SocketException se) {
            se.printStackTrace();
        }

        String ipAddress = "";
        for(String str : addresses) {
            ipAddress = ipAddress+str+"\n";
        }

        VerticalGroup vg = new VerticalGroup().space(3).pad(5).fill();
        vg.setBounds(0, 0, V_WIDTH, V_HEIGHT);

        labelDetails = new Label(ipAddress,skin);
        labelMessage = new Label("Hello World",skin);
        button = new TextButton("Send Message",skin);
        textIpAddress = new TextArea("",skin);
        textMessage = new TextArea("",skin);

        vg.addActor(labelDetails);
        vg.addActor(labelMessage);
        vg.addActor(textIpAddress);
        vg.addActor(textMessage);
        vg.addActor(button);

        stage.addActor(vg);

        stage.getCamera().viewportWidth = V_WIDTH;
        stage.getCamera().viewportHeight = V_HEIGHT;
        stage.getCamera().position.set(V_WIDTH/2,V_HEIGHT/2,0);
        new Thread((new Runnable() {
            @Override
            public void run() {
                ServerSocketHints ssHint = new ServerSocketHints();
                ssHint.acceptTimeout = 10000;
                com.badlogic.gdx.net.ServerSocket ss = Gdx.net.newServerSocket(Net.Protocol.TCP, 9021, ssHint);

                boolean loop = true;
                while(loop) {
                    Socket s = ss.accept(null);
                    BufferedReader buffer = new BufferedReader(new InputStreamReader(s.getInputStream()));

                    try{
                        labelMessage.setText(buffer.readLine());
                    }catch(IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }
        })).start();

        button.addListener(new ClickListener() {
            @Override public void clicked(InputEvent event, float x, float y) {
                String textToSend = "";
                if(textMessage.getText().length() == 0)
                    textToSend = "Doesn't say much but likes clicking buttons\n";
                else
                    textToSend = textMessage.getText()+"\n";

                SocketHints hints = new SocketHints();
                hints.connectTimeout=4000;
                Socket socket = Gdx.net.newClientSocket(Net.Protocol.TCP, textIpAddress.getText(), 9021, hints);
                try{
                    socket.getOutputStream().write(textToSend.getBytes());
                }catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        });
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        stage.draw();
        batch.end();
    }

    @Override
    public void resize(int width, int height) {}

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void dispose() {
        batch.dispose();
    }
}
