package net.ncguy.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.ncguy.core.Main;
import net.ncguy.commonutil.mail.MailInit;
import net.ncguy.core.network.Test;

public class DesktopLauncher {

    public static int fps = 60;

	public static void main (String[] arg) {
        try {
            if (arg.length > 0) {
                for (int i = 0; i < arg.length; i++) {
                    if (arg[i].equals("-l")) {
                        i++;
                        Main.level = arg[i];
                        Main.direct = true;
                    }
                    if (arg[i].equals("-d")) {
                        Main.isDebug = true;
                    }
                    if(arg[i].equals("-fps")) {
                        i++;
                        try{fps = Integer.valueOf(arg[i]);}catch(Exception e) {fps=60;}
                    }
                    if(arg[i].equals("-shader")) {
                        Main.hasShader = true;
                    }
                }
            }

            LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
            config.width = 1280;
            config.height = 720;
            config.fullscreen = false;
            config.vSyncEnabled = false;
            config.title = "I'mPossible";
            config.resizable = false;
            config.foregroundFPS = fps;
            new LwjglApplication(new Main(), config);
        }catch(Exception e) {
            MailInit.sendMail("ImPossible Issue: Error "+e.getCause(), e.getMessage());
            System.exit(0);
        }
	}
}
